/*
 *  BAIInterface.h
 *  baimobile
 *
 *  Copyright 2011 Biometric Associates, LP. All rights reserved.
 *
 */

#ifndef __BAIINTERFACE_H__
#define __BAIINTERFACE_H__

#include "BAI-defines-public.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * bai_interface_driver_version
 *
 * Returns the version number of the core SDK. The context parameter 
 * can be NULL. The version is represented by a 4 byte hex value.
 *
 */
unsigned int bai_interface_driver_version(hBAIContext* hContextOrNull);

/**
 * bai_interface_crypto_version
 *
 * Returns the version number of the crypto libraries. The context parameter 
 * can be NULL. The version is represented by a 4 byte hex value.
 *
 */
unsigned int bai_interface_crypto_version(hBAIContext* hContextOrNull);

/**
 * bai_interface_get_reader_firmware_version
 *
 * Returns the version of the reader (if known) or zero for error. If no
 * reader is connected then 0 is returned.  The version is represented by 
 * a 4 byte hex value.
 *
 * PARAMETERS:
 * hContext must be a valid handle.
 *
 */
unsigned int bai_interface_get_reader_firmware_version(hBAIContext* hContext);

/**
 * bai_interface_set_log_function
 *
 * Sets the user log function in the baiMobile library. You only need
 * to use this function if you want to capture logs (ie. for printing to 
 * the screen or a file). The logs will still go to the default log
 * console (Log Console for iOS, logcat for Android).
 *
 */
void bai_interface_set_log_function(userLogFn function);

/**
 * bai_interface_create_context
 * 
 * Initalize the BAI context and sets pContext to a pointer
 * to the context. Only call once per lifetime of the
 * application.
 *
 * May be called from any thread of your application. 
 *
 * Parameters:
 *  phContext: Pointer to a context pointer. If success then the context pointer
 *      will be initalized.
 *  os_context: Pointer to an OS-specific context object. For iOS use NULL.
 *
 * Return Value: BAIRESPONSE_SUCCESS on success.
 */
BAIRESPONSE bai_interface_create_context(hBAIContext** phContext, BAIContextInitOptions* options);

/**
 * bai_interface_connect
 * 
 * Establishes a secure connection to a BAI Mobile Bluetooth Reader.
 * Blocks until the connection is made and secured. This function does
 * not display any GUI. 
 * 
 * iOS Developers are encouraged to use the baiconnect module's connect
 * function (which also displays GUI) rather than this function directly.
 *
 * Function is blocking and must not be called from the main thread
 * of your Application. 
 *
 * PARAMETERS 
 *	options: RFU. Use 0x00
 *	addr_string: 18-byte bluetooth address in format (AA-BB-CC-DD-EE-FF)
 *		for architectures that require an address. NULL if address is not
 *		required. 
 *		Use NULL for iOS.
 *
 *	callback: Pointer to a callback function or NULL if no callbacks are
 *		desired. The callback function allows your Application to get informed
 *      of connection attempt state changes (See BAIConnectionAttemptState)
 *      which is useful for updating GUI. The callback function is called on
 *      the same thread that called bai_interface_connect() and should return
 *      quickly. 
 *
 *	canclFlag: NO LONGER SUPPORTED ON ANDROID (deprecated on iOS).  Use bai_interface_cancel_connect, instead.
 *		Boolean that is periodically checked in the connection code. If
 *		set to TRUE then the function will return BAIRESPONSE_CANCELLED_BY_USER.
 *		NOTE: This function may not return immediately after setting
 *		the cancel flag if the thread is inside a blocking system call. If 
 *      cancelFlag is non-NULL then it should initially point to a value of FALSE.
 *
 * RETURN VALUE: BAIRESPONSE_SUCCESS on success.
 *
 */
BAIRESPONSE bai_interface_connect(
											 hBAIContext* hContext,
											 unsigned long options,
											 const char* addr_string,
											 BAIConnectionAttemptStateChangeFunction callback,
											 bool* cancelFlag /*no longer supported on Android.*/
											 );

/**
* bai_interface_cancel_connect
*
* Non-blocking call to inform the driver that the connection attempt can be aborted
* An exiting call into bai_interface_connect (on another thread) would then return BAIRESPONSE_CANCELLED_BY_USER
* Such a return from a call into bai_interface_connect may not happen immediately.
*/
void bai_interface_cancel_connect(	hBAIContext* hContext );
	
	
	
	
/**
 * bai_interface_get_status_change
 *
 * Blocks until the provided state differs from the state of the
 * context. 
 *
 * SCardGetStatusChange() is the equivalent of this function in PC/SC.
 *
 * PARAMETERS: 
 *	knownState: The state that the calling code thinks is the current
 *		state. If BAICardStateUnknown is used then the function will 
 *		return immediately.
 *	newState: The state of the context when the function returns. 
 *
 * RETURN VALUE: An error code is returned if the context is not valid
 *		or if an internal error occurs. BAIRESPONSE_SUCCESS is returned
 *		for success.
 *
 * NOTES: 
 * Card state is a bit mask. See BAI-defines-public.h
 *
 * If the provided context is closed while a thread is inside of
 * this function then the behavior of the function is undefined.
 *
 */
BAIRESPONSE bai_interface_get_status_change(
                                            hBAIContext* hContext,
                                            BAICardState knownState,
                                            BAICardState* newState
                                            );
/**
 * bai_interface_get_card_state
 *
 * Returns the card state. 
 *
 * SCardGetStatusChange() is the equivalent of this function in PC/SC.
 * 
 * RETURN VALUE:
 * The return value is a bitmask of any of the following states:
 *  BAICardStateEmpty: The context is NULL, context is invalid, or no
 *      reader is connected.
 *  BAICardStateConnected: Reader is connected
 *  BAICardStateSecure: Connection is secure
 *
 */
BAICardState bai_interface_get_card_state(hBAIContext* hContext); 

#ifdef __cplusplus
}
#endif								   

#endif		// #ifndef __BAIINTERFACE_H__







