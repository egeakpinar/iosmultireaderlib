//
//  winscard_feitian.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#ifndef checkid_winscard_feitian_h
#define checkid_winscard_feitian_h

#define PCSC_API 

PCSC_API LONG SCa04EstablishContext(DWORD dwScope,
                                    /*@null@*/ LPCVOID pvReserved1, /*@null@*/ LPCVOID pvReserved2,
                                    /*@out@*/ LPSCARDCONTEXT phContext);

PCSC_API LONG SCa04ReleaseContext(SCARDCONTEXT hContext);

PCSC_API LONG SCa04IsValidContext(SCARDCONTEXT hContext);

PCSC_API LONG SCa04Connect(SCARDCONTEXT hContext,
                           LPCSTR szReader,
                           DWORD dwShareMode,
                           DWORD dwPreferredProtocols,
                           /*@out@*/ LPSCARDHANDLE phCard, /*@out@*/ LPDWORD pdwActiveProtocol);

PCSC_API LONG SCa04Reconnect(SCARDHANDLE hCard,
                             DWORD dwShareMode,
                             DWORD dwPreferredProtocols,
                             DWORD dwInitialization, /*@out@*/ LPDWORD pdwActiveProtocol);

PCSC_API LONG SCa04Disconnect(SCARDHANDLE hCard, DWORD dwDisposition);

PCSC_API LONG SCa04BeginTransaction(SCARDHANDLE hCard);

PCSC_API LONG SCa04EndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

PCSC_API LONG SCa04Status(SCARDHANDLE hCard,
                          /*@null@*/ /*@out@*/ LPSTR mszReaderName,
                          /*@null@*/ /*@out@*/ LPDWORD pcchReaderLen,
                          /*@null@*/ /*@out@*/ LPDWORD pdwState,
                          /*@null@*/ /*@out@*/ LPDWORD pdwProtocol,
                          /*@null@*/ /*@out@*/ LPBYTE pbAtr,
                          /*@null@*/ /*@out@*/ LPDWORD pcbAtrLen);

PCSC_API LONG SCa04GetStatusChange(SCARDCONTEXT hContext,
                                   DWORD dwTimeout,
                                   LPSCARD_READERSTATE rgReaderStates, DWORD cReaders);

PCSC_API LONG SCa04Control(SCARDHANDLE hCard, DWORD dwControlCode,
                           LPCVOID pbSendBuffer, DWORD cbSendLength,
                           /*@out@*/ LPVOID pbRecvBuffer, DWORD cbRecvLength,
                           LPDWORD lpBytesReturned);

PCSC_API LONG SCa04Transmit(SCARDHANDLE hCard,
                            const SCARD_IO_REQUEST *pioSendPci,
                            LPCBYTE pbSendBuffer, DWORD cbSendLength,
                            /*@out@*/ SCARD_IO_REQUEST *pioRecvPci,
                            /*@out@*/ LPBYTE pbRecvBuffer, LPDWORD pcbRecvLength);

PCSC_API LONG SCa04ListReaderGroups(SCARDCONTEXT hContext,
                                    /*@out@*/ LPSTR mszGroups, LPDWORD pcchGroups);

PCSC_API LONG SCa04ListReaders(SCARDCONTEXT hContext,
                               /*@null@*/ /*@out@*/ LPCSTR mszGroups,
                               /*@null@*/ /*@out@*/ LPSTR mszReaders,
                               /*@out@*/ LPDWORD pcchReaders);

PCSC_API LONG SCa04Cancel(SCARDCONTEXT hContext);

PCSC_API  LONG SCa04SetTimeout(SCARDCONTEXT hContext, DWORD dwTimeout);

PCSC_API LONG SCa04GetAttrib(SCARDHANDLE hCard, DWORD dwAttrId,
                             /*@out@*/ LPBYTE pbAttr, LPDWORD pcbAttrLen);

#define SCARD_PCI_T0	(&g_rgSCa04T0Pci) /**< protocol control information (PCI) for T=0 */
#define SCARD_PCI_T1	(&g_rgSCa04T1Pci) /**< protocol control information (PCI) for T=1 */
#define SCARD_PCI_RAW	(&g_rgSCa04RawPci) /**< protocol control information (PCI) for RAW protocol */
extern SCARD_IO_REQUEST g_rgSCa04T0Pci, g_rgSCa04T1Pci, g_rgSCa04RawPci;

#endif
