//
//  Response.h
//  iBankSpace
//
//  Created by Olivier Michiels on 08/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Response : NSObject
@property (nonatomic, strong) NSData *data;
@property (nonatomic) int rv;
@property (nonatomic) uint8_t sw1Ptr;
@property (nonatomic) uint8_t sw2Ptr;
@property (nonatomic, getter = isOk) BOOL ok;
@end
