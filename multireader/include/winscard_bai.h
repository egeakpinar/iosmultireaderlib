//
//  winscard_ibs.h
//  checkid
//
//  This file does not come with original bai SDK
//  This was created to allow direct access to SCard methods
//  
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#ifndef checkid_winscard_bai_h
#define checkid_winscard_bai_h

LONG SCa01EstablishContext(DWORD dwScope, LPCVOID pvReserved1,
                           LPCVOID pvReserved2, LPSCARDCONTEXT phContext);
LONG SCa01IsValidContext(SCARDCONTEXT hContext);

LONG SCa01ReleaseContext(SCARDCONTEXT hContext);


LONG SCa01SetTimeout(SCARDCONTEXT hContext, DWORD dwTimeout);

LONG SCa01Connect(SCARDCONTEXT hContext, LPCSTR szReader,
                  DWORD dwShareMode, DWORD dwPreferredProtocols, LPSCARDHANDLE phCard,
                  LPDWORD pdwActiveProtocol);

LONG SCa01Reconnect(SCARDHANDLE hCard, DWORD dwShareMode,
                    DWORD dwPreferredProtocols, DWORD dwInitialization,
                    LPDWORD pdwActiveProtocol);

LONG SCa01Disconnect(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa01BeginTransaction(SCARDHANDLE hCard);

LONG SCa01EndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa01CancelTransaction(SCARDHANDLE hCard);

LONG SCa01Status(SCARDHANDLE hCard, LPSTR mszReaderNames,
                 LPDWORD pcchReaderLen, LPDWORD pdwState,
                 LPDWORD pdwProtocol, LPBYTE pbAtr, LPDWORD pcbAtrLen);

LONG SCa01GetStatusChange(SCARDCONTEXT hContext, DWORD dwTimeout,
                          LPSCARD_READERSTATE_A rgReaderStates, DWORD cReaders);

LONG SCa01Control(SCARDHANDLE hCard, DWORD dwControlCode,
                  const void *pbSendBuffer, DWORD cbSendLength,
                  void *pbRecvBuffer, DWORD cbRecvLength, LPDWORD lpBytesReturned);
LONG SCa01Transmit(SCARDHANDLE hCard, LPCSCARD_IO_REQUEST pioSendPci,
                   LPCBYTE pbSendBuffer, DWORD cbSendLength,
                   LPSCARD_IO_REQUEST pioRecvPci, LPBYTE pbRecvBuffer,
                   LPDWORD pcbRecvLength);
LONG SCa01SecTransmit(SCARDHANDLE hCard,LPCBYTE pbSendBuffer, DWORD cbSendLength,LPBYTE pbRecvBuffer,LPDWORD pcbRecvLength);
LONG SCa01ListReaderGroups(DWORD hContext,
                           LPCBYTE *mszGroups, LPDWORD *pcchGroups);

LONG SCa01ListReaders(SCARDCONTEXT hContext, LPCSTR mszGroups,
                      LPSTR mszReaders, LPDWORD pcchReaders);

LONG SCa01Cancel(SCARDCONTEXT hContext);

// They are not public in the bai library
//#define SCARD_PCI_T0	(&g_rgSCa01T0Pci) /**< protocol control information (PCI) for T=0 */
//#define SCARD_PCI_T1	(&g_rgSCa01T1Pci) /**< protocol control information (PCI) for T=1 */
//#define SCARD_PCI_RAW	(&g_rgSCa01RawPci) /**< protocol control information (PCI) for RAW protocol */

//extern SCARD_IO_REQUEST g_rgSCa01T0Pci, g_rgSCa01T1Pci, g_rgSCa01RawPci;

// Because they are not public, we are relying on their values from another library here (identity)
#define SCARD_PCI_T0	(&g_rgSCa05T0Pci) /**< protocol control information (PCI) for T=0 */
#define SCARD_PCI_T1	(&g_rgSCa05T1Pci) /**< protocol control information (PCI) for T=1 */
#define SCARD_PCI_RAW	(&g_rgSCa05RawPci) /**< protocol control information (PCI) for RAW protocol */

extern SCARD_IO_REQUEST g_rgSCa05T0Pci, g_rgSCa05T1Pci, g_rgSCa05RawPci;


#endif
