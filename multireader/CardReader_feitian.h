//
//  CardReader_feitian.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 18/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardReader.h"

@interface CardReader_feitian : CardReader

#pragma mark - Public properties
@property(nonatomic, assign) BOOL force_main_thread;    // YES if operations should take place on main thread
@end
