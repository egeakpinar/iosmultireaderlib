/*
 *  bai-pcsc-controlcodes.h
 *  baimobile
 *
 *  Copyright 2011 Biometric Associates. All rights reserved.
 *
 */

#ifndef __BAI_PCSC_CONTROLCODES_H__
#define __BAI_PCSC_CONTROLCODES_H__

/**
 * Supported control codes for dwControlCode in SCardControl.
 * Response length or send data length is also defined
 */ 
// BAI_READER_COMMAND_COUNT_STATUS_WORDS is 2 bytes
#define BAI_CONTROL_FIRMWARE_VERSION					0xB0	// Reader firmware version
#define BAI_CONTROL_FIRMWARE_VERSION_SIZE_SEND			0
#define BAI_CONTROL_FIRMWARE_VERSION_SIZE_RECV			6		// 4 + BAI_READER_COMMAND_COUNT_STATUS_WORDS
#define BAI_CONTROL_BATTERY_STATUS						0xB1	// battery status
#define BAI_CONTROL_BATTERY_STATUS_SIZE_SEND			0
#define BAI_CONTROL_BATTERY_STATUS_SIZE_RECV			3		// 1 + BAI_READER_COMMAND_COUNT_STATUS_WORDS
#define BAI_CONTROL_SWITCH_TO_LOW_POWER_MODE			0xB5	// Low Power Mode
#define BAI_CONTROL_SWITCH_TO_LOW_POWER_MODE_SIZE_SEND	0
#define BAI_CONTROL_SWITCH_TO_LOW_POWER_MODE_SIZE_RECV	2		// BAI_READER_COMMAND_COUNT_STATUS_WORDS
#define BAI_CONTROL_RESET_CARD							0xB6	// Card Reset
#define BAI_CONTROL_RESET_CARD_SIZE_SEND				0
#define BAI_CONTROL_RESET_CARD_SIZE_RECV				38		// MAX_ATR_SIZE + BAI_READER_COMMAND_COUNT_STATUS_WORDS

#endif		// __BAI_PCSC_CONTROLCODES_H__



