/*
 *  BAI-defines-public.h
 *  baimobile
 *
 *  Copyright 2011 Biometric Associates. All rights reserved.
 *
 *
 *	Defines and types used throughout the SDK 
 */

#ifndef __BAI_DEFINES_PUBLIC__
#define __BAI_DEFINES_PUBLIC__

#include <stdarg.h>

////////////////////////////////////////////////////////////
// Misc types
typedef void* hBAIContext;	// context handle

#ifndef bool
#define bool unsigned char
#endif

////////////////////////////////////////////////////////////
// Return Codes
typedef unsigned char BAIRESPONSE;
#define BAIRESPONSE_SUCCESS					0x00
#define BAIRESPONSE_GENERIC_ERROR			0x44    // default error
#define BAIRESPONSE_SECURITY_VIOLATION		0x41
#define BAIRESPONSE_CANCELLED_BY_USER		0x4E
#define BAIRESPONSE_COMMIT_FAILURE			0x52
#define BAIRESPONSE_NO_CONNECTION			0x49
#define BAIRESPONSE_NO_TUNNEL				0x43
#define BAIRESPONSE_INVALID_PARAMETER       0x4F

////////////////////////////////////////////////////////////
//[NOTE] In the future, the names of these states may change to say "BAIReader.." instead of "BAICard.."
//[NOTE] The state 'BAICardStateConnecting is a place holder for a future state that is not yet implemented.
// Card State
typedef unsigned char BAICardState;
// Card States (Bitmask)
#define BAICardStateEmpty			0			// No reader or undefined state
#define BAICardStateConnecting		(1 << 0)	// A connection attempt has been started
#define BAICardStateConnected		(1 << 1)	// Bluetooth connected to reader and starting the crypto tunnel setup
#define BAICardStateSecure			(1 << 2)	// Tunnel Established.  Reader is ready for use.
#define BAICardStateExculsive       (1 << 3)	// Exclusive access by an app.
#define BAICardStateUnknown			0xFF		// Input only

////////////////////////////////////////////////////////////
// Logging. See Log.h or SDK documentation.
typedef unsigned long BAIModule_t;
typedef unsigned char BAILogLevel_t;
// Values for BAILogLevel_t
#define BAILogNone			0x00
#define BAILogInfo			0x01
#define BAILogDebug			0x02
#define BAILogCritical		0x04
#define BAILogError			0x08
#define BAILogAll			0xFF
// Values for BAIModule_t
#define BAIModuleNone				0x00000000			// Only used when setting BAILogVisibleLevels value
#define BAIModuleDriver				0x00000001
#define BAIModulePCSC				0x00000002			// PC/SC Interface
#define BAIModuleMiddleware         0x00000004			// SAME AS BAIModuleMWare
#define BAIModuleApp				0x00000008			// Application
#define BAIModuleContext			0x00000010
#define BAIModuleConnect			0x00000020
#define BAIModuleTransport			0x00000040	
#define BAIModuleReaderFirmware     0x00000080			
#define BAIModulePairing			0x00000100			// Pairing
#define BAIModuleCrypto				0x00000200			// Crypto
#define BAIModuleSpecial			0x00000400			// Reserved for debugging
#define BAIModuleInterface          0x00000800          // Prints when interface functions are called/return
#define BAIModuleAll				0xFFFFFFFF

// Format of the user log function.
typedef void (*userLogFn)(BAILogLevel_t level, BAIModule_t module, const char* format, va_list arg);

////////////////////////////////////////////////////////////
// Connection Callback Function
/**
 * The connection attempt callback function can be used to provide
 * the application with updates on the various steps of the connection
 * attempt. 
 *
 * If used, the function should be implemented by the Applications
 * code and return quickly since the thread calling the function
 * is the thread making the connection.
 */
#define	BAIConnectionAttemptStateNone				0							//Unused. Connection failure is indicated by return code from bai_interface_connect
#define	BAIConnectionAttemptStateConnecting			BAICardStateConnecting		//state is set immediately after a connection attempt is initiated
#define	BAIConnectionAttemptStateSecuringConnection	BAICardStateConnected		//state is set after finding the reader but before the secure tunnel is established
#define	BAIConnectionAttemptStateConnectedAndSecure	BAICardStateSecure			//Unused. Connection completion indicated by return code from bai_interface_connect
#define	BAIConnectionAttemptStateFailedToConnect	0							//Unused. Connection failure is indicated by return code from bai_interface_connect
#define	BAIConnectionAttemptStateFailedToSecure		0							//Unused. Connection failure is indicated by return code from bai_interface_connect

typedef unsigned int BAIConnectionAttemptState;
typedef void (*BAIConnectionAttemptStateChangeFunction)(BAIConnectionAttemptState state);

////////////////////////////////////////////////////////////
// Context init block
typedef struct BAIContextInitOptions {
    void* os_context;
    char* storage_path;     // NULL Terminated
} BAIContextInitOptions;

////////////////////////////////////////////////////////////
// iOS Notifications (NSNotifications)
// Accessory attachment and detachment
#define BAIAccessoryDidAttachNotification 			@"BAIAccessoryDidAttachNotification"
#define BAIAccessoryDidDetachNotification 			@"BAIAccessoryDidDetachNotification"
// Notification includes a BAICardState inside an NSNumber object (unsigned int)
#define BAICardStateChangeNotification				@"BAICardStateChangeNotification"
// Request for a navigation controller. See notes in SDK documentation.
#define BAIRequestNavControllerNotification			@"BAIRequestNavControllerNotification"
// Pairing finished (success) or GUI dismissed by user (failure).
// The object of the notification is a NSNumber (boolValue) indicates if successful
#define BAIPairingDidFinishNotification				@"BAIPairingDidFinishNotification"

////////////////////////////////////////////////////////////

#endif	// __BAI_DEFINES_PUBLIC__



