/*
 *  BAIModulePairing.h
 *  baimodules-ios-sdk
 *
 *  Copyright 2011 Biometric Associates. All rights reserved.
 *
 */

#ifndef __BAIINTERFACE_IOS_PAIR_H__
#define __BAIINTERFACE_IOS_PAIR_H__

#include <Foundation/Foundation.h>
#include <UIKit/UIKit.h>
#include "BAI-defines-public.h"

#ifdef __cplusplus
extern "C" {
#endif
		
typedef void (*BAIPairingCallback)(BOOL success);	
	
/**
 * Pushes the pairing view controller onto the given controller. 
 * Use the BAIPairingDidFinishNotification notification to 
 * get informed of when the pairing is successful or if the
 * user dismisses the GUI. A callback function can also be specified
 * to be informed of the pairing status
 * 
 * PARAMETERS:
 *	context:	An established context. The context must remain
 *				established while a thread is calling into this
 *				function.
 *	controller:	A UINavigationController that is used to push
 *				the connection view controller.
 *
 *	callbackOrNull: Specify a callback function to be informed of
 *				when pairing is success or failure (user dismiss
 *				the pairing view). Use NULL if you don't use the
 *				callback. 
 *
 *
 * 
 * RETURN VALUE: 
 *	BAIRESPONSE_SUCCESS : Success
 *	BAIRESPONSE_CANCELLED_BY_USER : User dismissed the UI
 *	Or any other error code that is listed.
 *
 *
 * RESOURCES: This function requires that the NSBundle with
 *	identifer com.baimobile.resources is present within the
 *	Application. 
 *
 */
BAIRESPONSE bai_interface_show_pairing_view(
											hBAIContext* hContext,
											UINavigationController* controller,
											BAIPairingCallback callbackOrNull	   
											);
	
unsigned int bai_interface_pairing_module_version();
#ifdef __cplusplus
}
#endif


#endif // __BAIINTERFACE_IOS_PAIR_H__
