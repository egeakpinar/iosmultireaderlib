/*
 *  BAIInterface_iOS.h
 *  baimobile
 *
 *  Copyright 2011 Biometric Associates. All rights reserved.
 *
 */

#ifndef __BAIINTERFACE_IOS_H__
#define __BAIINTERFACE_IOS_H__

#include "BAI-defines-public.h"
#include <Foundation/Foundation.h>

/**
 * bai_interface_accessory_properties
 *
 * Returns an NSDictionary with various properties of the attached
 * accessory or NULL if no BAI accessory is attached. 
 *
 * Parameters:
 *		context: A context created by bai_interface_create_context
 *
 */
NSDictionary* bai_interface_accessory_properties(hBAIContext* hContext);

/**
 * bai_interface_accessory_is_paired
 *
 * Returns whether the attached accessory is paired, or FALSE if 
 * no BAI accessory is attached.
 *
 * Parameters: 
 *		context: A context created by bai_interface_create_context
 *
 */
BOOL bai_interface_accessory_is_paired(hBAIContext* hContext);

/**
 * bai_interface_accessory_is_attached
 *
 * Returns whether a BAI accessory is attached
 *
 */
BOOL bai_interface_accessory_is_attached(hBAIContext* hContext);

/** 
 * bai_interface_delete_pairing
 *
 * Deletes the pairing information stored in the attached accessory
 * 
 */
BAIRESPONSE bai_interface_delete_pairing(hBAIContext* hContext);

#endif		// __BAIINTERFACE_IOS_H__

