//
//  CardReader.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 21/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader.h"
#import "Response.h"
#import "CardCommunication.h"

@implementation CardReader

@synthesize ATR = _ATR;
@synthesize active_protocol = _active_protocol;

#pragma mark - Properties
-(uint8_t)blockSize {
    return 0x80;
}

-(void)logMessage:(NSString*) message {
    NSLog(@"%@", message);
}

-(void*)getContext {
    return NULL;
}

-(void)logResponseCode:(Response*)response {
    [self logMessage:[NSString stringWithFormat:@"Response code is: %02x%02x", response.sw1Ptr, response.sw2Ptr]];
}

- (protocol) getActiveProtocol  {
    if(_active_protocol == PCI_PROTOCOL_Undefined)  {
        NSLog(@"Active protocol undefined, ensure you have powered ON card at least once");
    }
    
    return _active_protocol;
}

- (NSString *) getATR {
    if(_ATR == nil) {
        NSLog(@"ATR is not determined, ensure you have powered ON card at least once");
    }
    return _ATR;
}

-(Response*)sendCommand:(NSString *)command {
    [[NSNotificationCenter defaultCenter] postNotificationName:CCNOTIFICATION_STATUS_CHANGE object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@ID_CARD_DID_READ, @"card_status", nil]];
    
    return nil;
}

-(BOOL)isSignaturePadded {
    return YES;
}
#pragma mark - Abstract methods


#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}


- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    NSLog(@"Unimplemented SCard method");
    return SCARD_E_READER_UNSUPPORTED;
}


@end
