//
//  T1Command.h
//  iSmartSDK
//
//  Created by Chris Powell on 11/3/11.
//  Copyright (c) 2011 CHARGE Anywhere LLC. All rights reserved.
//

#import "ISO7816Command.h"

@interface T1Command : ISO7816Command{
  unsigned char nodeAddressByte;
  unsigned char protocolControlByte;
  unsigned char lengthByte;
  NSData *informationFieldBytes;
  unsigned char edcByte;
}

@property(nonatomic, assign) unsigned char nodeAddressByte;
@property(nonatomic, assign) unsigned char protocolControlByte;
@property(nonatomic, assign) unsigned char lengthByte;
@property(nonatomic, retain) NSData *informationFieldBytes;
@property(nonatomic, readonly) unsigned char edcByte;

@end
