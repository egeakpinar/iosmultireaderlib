/*
 * Copyright (c) 2005 - 2011, Precise Biometrics AB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Precise Biometrics AB nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */
 
#import <Foundation/Foundation.h>

#import <ExternalAccessory/ExternalAccessory.h>

/** Accessory protocol that may be used to get notifications when the accessory
  * is being connected and disconnected. */
@protocol PBAccessoryDelegate <NSObject>

@optional

/* Method that will be called when the accessory connects to the device. */
- (void)pbAccessoryDidConnect;

/* Method that will be called when the accessory disconnects from the device. */
- (void)pbAccessoryDidDisconnect;

@end

/** Class that handles communication with the Tactivo accessory that
  * is not biometry or smartcard related. */
@interface PBAccessory : NSObject <EAAccessoryDelegate> {
    /* Tells if the accessory is connected or not. */
    BOOL connected;
    
    /* The delegates of the PBAccessory. */
    NSMutableArray* delegates;
}

@property (nonatomic, readonly, getter = isConnected) BOOL connected;

/* Class method for receiving the singleton object. */
+ (PBAccessory*) sharedClass;

/* Adds a delegate. */
- (void)addDelegate: (id<PBAccessoryDelegate>) delegate;

/* Removes a delegate. */
- (void)removeDelegate: (id<PBAccessoryDelegate>) delegate;

/** Returns the EAAccessory object for the connected accessory, if any. 
  * 
  * @return the connected accessory, or nil if no accessory is connected.
  */
- (EAAccessory*)getAccessory;

@end