//
//  winscard_ibs.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#ifndef checkid_winscard_ibs_h
#define checkid_winscard_ibs_h

LONG SCa05EstablishContext(DWORD dwScope, LPCVOID pvReserved1,
                           LPCVOID pvReserved2, LPSCARDCONTEXT phContext);
LONG SCa05IsValidContext(SCARDCONTEXT hContext);

LONG SCa05ReleaseContext(SCARDCONTEXT hContext);


LONG SCa05SetTimeout(SCARDCONTEXT hContext, DWORD dwTimeout);

LONG SCa05Connect(SCARDCONTEXT hContext, LPCSTR szReader,
                  DWORD dwShareMode, DWORD dwPreferredProtocols, LPSCARDHANDLE phCard,
                  LPDWORD pdwActiveProtocol);

LONG SCa05Reconnect(SCARDHANDLE hCard, DWORD dwShareMode,
                    DWORD dwPreferredProtocols, DWORD dwInitialization,
                    LPDWORD pdwActiveProtocol);

LONG SCa05Disconnect(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa05BeginTransaction(SCARDHANDLE hCard);

LONG SCa05EndTransaction(SCARDHANDLE hCard, DWORD dwDisposition);

LONG SCa05CancelTransaction(SCARDHANDLE hCard);

LONG SCa05Status(SCARDHANDLE hCard, LPSTR mszReaderNames,
                 LPDWORD pcchReaderLen, LPDWORD pdwState,
                 LPDWORD pdwProtocol, LPBYTE pbAtr, LPDWORD pcbAtrLen);

LONG SCa05GetStatusChange(SCARDCONTEXT hContext, DWORD dwTimeout,
                          LPSCARD_READERSTATE_A rgReaderStates, DWORD cReaders);

LONG SCa05Control(SCARDHANDLE hCard, DWORD dwControlCode,
                  const void *pbSendBuffer, DWORD cbSendLength,
                  void *pbRecvBuffer, DWORD cbRecvLength, LPDWORD lpBytesReturned);
LONG SCa05Transmit(SCARDHANDLE hCard, LPCSCARD_IO_REQUEST pioSendPci,
                   LPCBYTE pbSendBuffer, DWORD cbSendLength,
                   LPSCARD_IO_REQUEST pioRecvPci, LPBYTE pbRecvBuffer,
                   LPDWORD pcbRecvLength);
LONG SCa05SecTransmit(SCARDHANDLE hCard,LPCBYTE pbSendBuffer, DWORD cbSendLength,LPBYTE pbRecvBuffer,LPDWORD pcbRecvLength);
LONG SCa05ListReaderGroups(DWORD hContext,
                           LPCBYTE *mszGroups, LPDWORD *pcchGroups);

LONG SCa05ListReaders(SCARDCONTEXT hContext, LPCSTR mszGroups,
                      LPSTR mszReaders, LPDWORD pcchReaders);

LONG SCa05Cancel(SCARDCONTEXT hContext);

#define SCARD_PCI_T0	(&g_rgSCa05T0Pci) /**< protocol control information (PCI) for T=0 */
#define SCARD_PCI_T1	(&g_rgSCa05T1Pci) /**< protocol control information (PCI) for T=1 */
#define SCARD_PCI_RAW	(&g_rgSCa05RawPci) /**< protocol control information (PCI) for RAW protocol */

extern SCARD_IO_REQUEST g_rgSCa05T0Pci, g_rgSCa05T1Pci, g_rgSCa05RawPci;


#endif
