//
//  Common.h
//  YAction
//
//  Created by Ismail Ege AKPINAR on 06/01/2013.
//  Copyright (c) 2013 Quiet Riots. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SUPER_SEND(X) [super sendCommand:X];

#define EXC_BEGIN @try {
#define EXC_END     } \
@catch (id obj) { \
    if([obj isKindOfClass:[NSException class]]) { \
        NSException *exc = obj; \
        NSLog(@"Warning - Exception %@, %@", exc.name, exc.reason); \
    } \
    else    { \
        NSLog(@"Warning - Exception %@",obj); \
    } \
}



@interface CCCommon : NSObject

// YES if this is the first time this app is launched
+ (BOOL) is_first_load;

+ (unsigned char *) stringToByteArray:(NSString *)input outputLength:(int *)outputLength;
+ (NSString *) dataToString:(NSData *)data;
+ (NSData *) stringToData:(NSString *)input;
+ (NSString *) string_trimmed:(NSString *)input;
@end
