//
//  Response.m
//  iBankSpace
//
//  Created by Olivier Michiels on 08/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Response.h"

#define OK @"<90><00>"

@implementation Response
@synthesize data = _data;
@synthesize rv = _rv;
@synthesize sw1Ptr = _sw1Ptr;
@synthesize sw2Ptr = _sw2Ptr;

-(BOOL)isOk {
    return self.sw1Ptr == 0x90 && self.sw2Ptr == 0x00;
}
@end
