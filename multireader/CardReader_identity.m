//
//  CardReader_identity.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 13/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_identity.h"
#import "CCCommon.h"

#import "winscard_identity.h"
#import "wintypes.h"
#import "IDCodes.h"

#if !(TARGET_IPHONE_SIMULATOR)
@implementation CardReader_identity {
    SCARDHANDLE _hCard;
	SCARDCONTEXT _hContext;
	DWORD _dwPref;
    
    int _currentReaderStatus;
    int _readerCount;
    SCARD_READERSTATE _rgReaderStates[1];
    
    BOOL _is_first_time;
}

@synthesize ATR = _ATR;
@synthesize atr = _atr;

#pragma mark - CardReader properties

#pragma mark - CardReader methods
- (void) config {
    EXC_BEGIN
    self.type = identity;
    _is_first_time = YES;
    if(_hContext==0) {
        LONG rv = SCa05EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_hContext);
        if(rv)  {
            NSLog(@"SCardEstablishContext Error:0x%x",rv);
        }
        else    {
            NSLog(@"SCardEstablishContext Successful");
        }
    }
    else    {
        NSLog(@"SCardEstablishContext already initialized ?");
    }
    EXC_END
}

- (void) reset  {
    EXC_BEGIN
    if(_hContext)    {
        SCa05ReleaseContext(_hContext);
        _hContext=0;
        _hCard=0;
    }
    EXC_END
    @finally {
        _is_first_time = YES;
    }
}

- (ID_CODE)connectCard {
    EXC_BEGIN
    LONG rv;
    
    if(_is_first_time)   {
        _readerCount = 0;
        if(!_hContext)   {
            rv = SCa05EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_hContext);
            if(rv)
            {
                return ID_READER_CONTEXT_ERROR;
            }
            else    {
                NSLog(@"SCardEstablishContext successful");
            }
        }
        
        // Get reader name
        char mszReaders[128] = "";
        DWORD dwReaders = -1;
        
        rv = SCa05ListReaders(_hContext, NULL, mszReaders, &dwReaders);
        if(rv != SCARD_S_SUCCESS) {
            NSLog(@"SCardListReaders error %08x",rv);
        }
        
        NSLog(@"Reader name %s", mszReaders);
        
        rv=SCa05GetStatusChange(_hContext,INFINITE, 0, _readerCount);
        
        if(rv!=SCARD_S_SUCCESS) {
            return ID_CARD_CONNECT_ERROR;
        } else {
            // No card, continue below
        }
        
        _readerCount=1;
        _rgReaderStates[0].szReader = mszReaders;
        _rgReaderStates[0].dwCurrentState = SCARD_STATE_EMPTY;
        
        _is_first_time = NO;
    }
    
    rv = SCa05GetStatusChange(_hContext, INFINITE, _rgReaderStates, _readerCount);
    if(rv!=SCARD_S_SUCCESS) {
        _readerCount=0;
        _rgReaderStates[0].dwCurrentState=SCARD_STATE_EMPTY;
        return ID_READER_ABSENT;
    } else {
        _readerCount=1;
        if(_rgReaderStates[0].dwEventState & SCARD_STATE_PRESENT)    {
            _rgReaderStates[0].dwCurrentState=SCARD_STATE_PRESENT;
            
            return ID_CARD_ACCESS_SUCCESSFUL;
        } else {
            _rgReaderStates[0].dwCurrentState=SCARD_STATE_EMPTY;
            return ID_CARD_ABSENT;
        }
    }
    EXC_END
}


- (ID_CODE) powerOnCard {
    EXC_BEGIN
    LONG rv;
    rv = SCa05Connect(_hContext, _rgReaderStates[0].szReader,
                      SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
                      &_hCard, &_dwPref);
    if(rv!=SCARD_S_SUCCESS) {
        // NOTE: For card status, we are always relying on connectCard response for sake of simplicity (separation of concerns)
        NSLog(@"SCardConnect error %x", rv);
        return ID_CARD_CONNECT_ERROR;
    }
    else    {
        if(_hCard)   {
            DWORD dwReaderLen = MAX_READERNAME;
            char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
            DWORD dwAtrLen = MAX_ATR_SIZE;
            DWORD dwState,dwProt;
            unsigned char pbAtr[MAX_ATR_SIZE];
            
            rv = SCa05Status(_hCard, pcReaders, &dwReaderLen, &dwState, &dwProt,
                             pbAtr, &dwAtrLen);
            
            switch (dwProt) {
                case SCARD_PROTOCOL_ANY:
                case SCARD_PROTOCOL_T0:
                    self.active_protocol = PCI_PROTOCOL_0;
                    break;
                case SCARD_PROTOCOL_T1:
                    self.active_protocol = PCI_PROTOCOL_1;
                    break;
                case SCARD_PROTOCOL_RAW:
                    self.active_protocol = PCI_PROTOCOL_Raw;
                    break;
                default:
                    NSLog(@"Unrecognised protocol");
                    self.active_protocol = PCI_PROTOCOL_Undefined;
            }
            
            NSData *data_atr = [NSData dataWithBytes:pbAtr length:dwAtrLen];
            _ATR = [CCCommon dataToString:data_atr];
            
            NSLog(@"ATR %@", [NSData dataWithBytes:pbAtr length:dwAtrLen]);
            NSLog(@"Current reader protocol %d", dwProt - 1);
            return ID_OK;
        }
        return ID_CARD_CONNECT_ERROR;
    }
    EXC_END
}

- (ID_CODE) powerOffCard    {
    EXC_BEGIN
    if(_hCard)  {
        LONG rv = SCa05Disconnect(_hCard, SCARD_RESET_CARD);
        _hCard = 0;
    }
    // rv is ignored, assuming success
    return ID_OK;
    EXC_END
}

- (Response *) sendCommand:(NSString *)command {
    EXC_BEGIN
    
    SUPER_SEND(command)
    Response *response = [[Response alloc] init];
	uint8_t pbRecvBuffer[258];
	DWORD cbRecvLength=258;
	LONG rv;
	SCARD_IO_REQUEST pioRecvPci;
 	SCARD_IO_REQUEST pioSendPci;

	switch (self.active_protocol) {
        case PCI_PROTOCOL_0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case PCI_PROTOCOL_1:
            pioSendPci = *SCARD_PCI_T1;
            break;
        case PCI_PROTOCOL_Raw:
            pioSendPci = *SCARD_PCI_RAW;
            break;
        case PCI_PROTOCOL_Undefined:
        default:
            NSLog(@"Active protocol not set");
            break;
    }

    int pbSendLength = -1;
    uint8_t *pbSendBuffer = [CCCommon stringToByteArray:command outputLength:&pbSendLength];
    if(pbSendBuffer == NULL)    {
        NSLog(@"Could not create command bytes");
        return nil;
	}
    
	rv=SCa05Transmit(_hCard,&pioSendPci,
					 pbSendBuffer, pbSendLength,
					 &pioRecvPci,pbRecvBuffer,
					 &cbRecvLength);
    response.rv = rv;
	if(rv==SCARD_S_SUCCESS) {
        NSLog(@"SCardTransmit successful");
        NSLog(@"%@",[NSData dataWithBytes:pbRecvBuffer length:cbRecvLength]);
        response.data = [NSData dataWithBytes:pbRecvBuffer length:cbRecvLength-2];
        response.sw1Ptr = pbRecvBuffer[cbRecvLength - 2];
        response.sw2Ptr = pbRecvBuffer[cbRecvLength - 1];
	} else {
        NSLog(@"SCardTransmit Error:%x", rv);
	}
    return response;
    EXC_END
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    EXC_BEGIN
    return SCa05EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
    EXC_END
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;
    return SCa05IsValidContext(hContext);
    EXC_END    
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;
    return SCa05ReleaseContext(hContext);
    EXC_END    
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;    
    return SCa05SetTimeout(hContext, dwTimeout);
    EXC_END    
}

- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;    
    return SCa05Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
    EXC_END    
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;

    return SCa05Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
    EXC_END    
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;
    
    return SCa05Disconnect(hCard, dwDisposition);
    EXC_END    
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05BeginTransaction(hCard);
    EXC_END    
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;
    return SCa05EndTransaction(hCard, dwDisposition);
    EXC_END    
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05CancelTransaction(hCard);
    EXC_END    
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
    EXC_END    
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;    
    return SCa05GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
    EXC_END    
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
    EXC_END    
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
    EXC_END    
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    EXC_BEGIN
    if(!hCard)  hCard = _hCard;    
    return SCa05SecTransmit(hCard, pbSendBuffer, cbSendLength, pbRecvBuffer, pcbRecvLength);
    EXC_END    
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    EXC_BEGIN
    NSLog(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
    EXC_END    
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;    
    return SCa05ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
    EXC_END    
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    if(!hContext) hContext = _hContext;
    return SCa05Cancel(hContext);
    EXC_END    
}

@end
#endif
