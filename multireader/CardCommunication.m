//
//  CardCommunication.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 14/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardCommunication.h"
#import "CardReader.h"
#import <ExternalAccessory/ExternalAccessory.h>
#import "CardReader_identity.h"
#import "CardReader_feitian.h"
#import "CardReader_precise.h"
#import "CardReader_idtech.h"
#import "CardReader_bai.h"
#import <pthread.h>
#import <objc/runtime.h>
#import "IDCodes.h"
#import "CCCommon.h"

@interface CardCommunication()


@property (nonatomic, assign) pthread_t pthread_background;    // pthread pointer to thread_background
@property (nonatomic, retain) NSThread *thread_current; // Reference to current thread (this is not main thread)
@property NSMutableArray *arr_timed_notifications;  // This array is for timed state change notifications to be posted
@property(nonatomic, retain) NSTimer *timer_poll_status;  // Timer used to poll status

// EAAccessory notifications should be handled by this thread instead
@property NSMutableArray *arr_ea_notifications; // This array holds EAAccessory notifications received on main thread
@property NSThread *notificationThread;
@property NSLock *notificationLock;
@property NSMachPort *notificationPort;

// Timeout methods
#define TIMEOUT_LIMIT 3 // Kill a thread after 3 subsequent timeout warnings
@property (nonatomic, assign) int timeout_counter;
@property (nonatomic, retain) NSLock *lock_timeout_counter;

// Protocols
@property (nonatomic, retain) NSArray *protocols;
@end

@implementation CardCommunication



// Singleton instance
static CardCommunication *_instance = nil;

+ (id) sharedWithSupportedProtocols:(NSArray*)protocols {
    // Note that this instance will be reset with each start/stop
    // hence dispatch_once should not be used here
    if(!_instance)  {
        _instance = [[CardCommunication alloc] initWithSupportedProtocols:protocols];
        // Set to defaults
        [_instance setIs_pooled_notification_mode:YES];
        [_instance setIs_timeout_enabled:YES];
    }
    return _instance;
}

#pragma mark - Initialize
-(id)initWithSupportedProtocols:(NSArray*)protocols {
    self = [super init];
    if (self) {
        self.protocols = protocols;
    }
    return self;
}
#pragma mark - Thread control methods

// Should be started only once during application runtime
- (void) start  {
    @autoreleasepool {
        // Register for accessory notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
        
        [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
        
        // Set to initial status
        [self setReader_status:ID_READER_ABSENT];
        [self setCard_status:ID_CARD_ABSENT];
        
        
        
        // Initialise instance variables
        _arr_timed_notifications = [[NSMutableArray alloc] init];
        _thread_current = [NSThread currentThread];
        _lock_timeout_counter = [[NSLock alloc] init];
        _timeout_counter = 0;
        
        [self setupThreading];
        
        // Check if an accessory is already connected
        // (Note that this must be executed after setupThreading finishes, to be able to
        // process EA notifications properly)
        // (Note: It's safer to trigger this slightly after app launch, otherwise EA sometimes fails to read protocol string)
        [self performSelector:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil afterDelay:2.0f];
        
        NSTimer *timer_check;
        if(_is_timeout_enabled) {
            // Setup a signal handler for our custom signal (will be used to terminate thread in case of timeout)
            signal(SIGUSR1, signalHandler);
                    
            // Add a timer to check timeout
            timer_check = [NSTimer scheduledTimerWithTimeInterval:CC_POLLING_PERIOD_S * 10 target:self selector:@selector(checkTimeOut) userInfo:nil repeats:YES];
        }

        // Loop infinitely
        do  {
            // Start the run loop but return after each source is handled.
            SInt32    result = CFRunLoopRunInMode(kCFRunLoopDefaultMode, CC_POLLING_PERIOD_S * 10, YES);
            
            // If a source explicitly stopped the run loop, or if there are no
            // sources or timers, go ahead and exit.
            if ((result == kCFRunLoopRunStopped) || (result == kCFRunLoopRunFinished))
                break;
            else if([[NSThread currentThread] isCancelled]) {
                if(_thread_background && [_thread_background isExecuting])    {
                    NSLog(@"wont cancel CC yet, waiting for SDK thread to be cancelled first");
                }
                else    {
                    NSLog(@"cancelling CC (SDK thread is cancelled already)");
                    break;
                }

            }
            
            // Check for any other exit conditions here and set the
            // done variable as needed.
        }
        while (true);

        // Invalidate timeout check timer
        if(timer_check) {
            [timer_check invalidate];
        }
        
        NSLog(@"Terminating CardCommunication thread");
    }
}

- (void) stop   {
    // Unregister from accessory notifications
    [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Stop thread (if active)
    if(_thread_background && [_thread_background isExecuting])  {
        [_thread_background cancel];
    }
    
    [_reader reset];
    
    _instance = nil;    // Reset singleton instance
}

// Opens a MachPort to receive notifications received on the main thread
// (we want to process on this thread instead but by default, they get delivered to the main thread)
- (void) setupThreading {
    if (_arr_ea_notifications) {
        return;
    }
    _arr_ea_notifications      = [[NSMutableArray alloc] init];
    _notificationLock   = [[NSLock alloc] init];
    _notificationThread = [NSThread currentThread];
    
    _notificationPort = [[NSMachPort alloc] init];
    [_notificationPort setDelegate:self];
    [[NSRunLoop currentRunLoop] addPort:_notificationPort
                                forMode:(NSString*)kCFRunLoopCommonModes];
}

- (void) checkTimeOut   {
    if(!_thread_background || ![_thread_background isExecuting])  {
        NSLog(@"checkTimeOut - background thread is not active yet, no need to check for timeout");
        return;
    }
    [_lock_timeout_counter lock];
    if(_timeout_counter == -1)    {
        _timeout_counter = 0;
    }
    else    {
        _timeout_counter += 1;
    }
    NSLog(@"checkTimeOut - counter is %d", _timeout_counter);
    
    int timeout_limit = TIMEOUT_LIMIT;
    if(_reader.type == bai) {   // bai is very slow sometimes, give it more time before timeout
        timeout_limit *= 3;
    }
    if(_timeout_counter == timeout_limit)   {
        NSLog(@"Something must be hung!!!!!");
        [_timer_poll_status invalidate];    // Invalidate because it's repeating
        if(!_pthread_background)    {
            NSLog(@"Failed to kill thread - pthread pointer not set");
            return;
        }
        int ret = pthread_kill(_pthread_background, SIGUSR1);
        if(ret == 0)    {
            NSLog(@"Killing hung thread - successful");
            [self resetReader];
            _timeout_counter = 0;
            // An accessory might be connected in the meanwhile, give it 3 seconds
            [self performSelector:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil afterDelay:3];
        }
        else if(ret == EINVAL)   {
            NSLog(@"Failed to kill thread - unsupported signal");
        }
        else if(ret == ESRCH)   {
            NSLog(@"Failed to kill thread - invalid thread");
        }
        else    {
            NSLog(@"Failed to kill thread -  return code %d", ret);
        }
    }
    [_lock_timeout_counter unlock];
}

/* 
 This signalHandler intercepts SIGUSR1 custom signal and kills the thread
 This is used to force kill a thread if it is suspected to hang
 Note that this is not ideal because we're using pthread_kill and pthread_exit methods to kill an
 NSThread object. An NSThread object is a complex wrapper for a pthread and killing through pthread methods
 prevent it from freeing memory gracefully, leaving uncollected memory garbage behind.
 For our purposes, [NSThread cancel] is not sufficient because we don't have control over SDK calls we make
 */
void signalHandler(int signal) {
    printf("\n\nForce kill thread\n\n");
    printf("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    pthread_exit(NULL);
}

#pragma mark - NSMachPort delegate

- (void) handleMachMessage:(void *)msg  {
    [_notificationLock lock];
    
    while ([_arr_ea_notifications count]) {
        NSNotification *notification = [_arr_ea_notifications objectAtIndex:0];
        [_arr_ea_notifications removeObjectAtIndex:0];
        [_notificationLock unlock];
        [self processNotification:notification];
        [_notificationLock lock];
    };
    
    [_notificationLock unlock];
}


#pragma mark - Properties
- (void) setReader_status:(long)new_reader_status   {
    long old_value = _reader_status;
    _reader_status = new_reader_status;
    if(new_reader_status != old_value) {
        
        if(new_reader_status != ID_READER_PROBLEM)    {
            // Reset error message if there is no error
            _reader_status_error_message = nil;
        }

        [self notifyStateChange];
    }
}

- (void) setCard_status:(long)new_card_status {
    long old_value = _card_status;
    _card_status = new_card_status;
    if(new_card_status != old_value) {
        
        if(new_card_status != ID_CARD_CONNECT_ERROR)    {
            // Reset error message if there is no error
            _card_status_error_message = nil;
        }
        
        [self notifyStateChange];
    }
}

#pragma mark - EAAccessory notifications


- (void) processNotification:(NSNotification *) notification    {
    if ([NSThread currentThread] != _thread_current) {
        // Forward the notification to the correct thread.
        NSLog(@"forwarding notification");
        [_notificationLock lock];
        [_arr_ea_notifications addObject:notification];
        [_notificationLock unlock];
        [_notificationPort sendBeforeDate:[NSDate date]
                                   components:nil
                                         from:nil
                                     reserved:0];
    }
    else {
        // Process the notification here
        if([notification.name isEqualToString:EAAccessoryDidConnectNotification]) {
            if(_thread_background && [_thread_background isExecuting])  {
                NSLog(@"A thread is already active, won't do anything");
                return;
            }
            
            [self setReader_status:ID_READER_PRESENT];
            
            // Check if accessory is supported
            EAAccessory *connectedAccessory = [[notification userInfo] objectForKey:EAAccessoryKey];
            
            [self resetReader];
            
            NSLog(@"examining protocols of connected accessory");
            BOOL found = NO;
            for(NSString *protocol in connectedAccessory.protocolStrings)   {
                NSLog(@"protocol ||%@||", protocol);
                for (NSString *supportedProtocol in self.protocols) {
                    if ([[protocol lowercaseString] isEqualToString:[supportedProtocol lowercaseString]]) {
                        found = YES;
                        _reader = [self cardReaderFromAccessoryProtocol:protocol];
                        if(_reader) {
                            NSLog(@"Supported card reader found");
                            break;
                        }
                    }
                }
            }
                        
            if(!connectedAccessory.protocolStrings || [connectedAccessory.protocolStrings count] <= 0)    {
                [self setReader_status:ID_READER_TRYING];
                return;
            }
            
            if(!_reader)    {
                [self setReader_status:ID_READER_NOT_VALID];
                [self setReader_status_error_message:@"Card reader not supported"];
            }
            else    {
                NSLog(@"Great, card reader supported");
                [self setReader_status:ID_READER_TRYING];
                
                // Start polling for card connection
                // (Although we do this check above, it's safe to do it here as well, just to avoid race conditions)
                if(_thread_background && [_thread_background isExecuting])  {
                    NSLog(@"A thread is already active, won't do anything 2");
                }
                else    {
                    NSLog(@"Starting a thread for pollCardStatus");
                    
                    _thread_background = [[NSThread alloc] initWithTarget:self selector:@selector(pollCardStatus) object:nil];
                    [_thread_background start];
                    
                }
            }
        }
        else if([notification.name isEqualToString:EAAccessoryDidDisconnectNotification])   {
            // TODO: If there is more than one accessory, this will fail
            // Replace this with AccessoryDelegate specific to used accessory
            
            [self setReader_status:ID_READER_ABSENT];
            [self setCard_status:ID_CARD_ABSENT];
            
            BOOL is_thread_running = NO;
            if(_thread_background && [_thread_background isExecuting])  {
                is_thread_running = YES;
                [_thread_background cancel];
            }
            
            if(is_thread_running)   {
                NSLog(@"will try to terminate thread");
                if(_thread_background && [_thread_background isExecuting])   {
                    [_thread_background cancel];
                }
                // Allow some time for thread to cancel
                usleep(500);
            }
            
            // Reset if thread stopped executing
            if(is_thread_running && ![_thread_background isExecuting])  {
                NSLog(@"thread terminated easily, will reset reader");
                [self resetReader];
            }
            else if(is_thread_running)   {
                NSLog(@"thread did NOT terminate easily, will rely on the timeout mechanism");
            }
            // else, in our force kill mechanism, the above will be called
        }
        
    }
}

-(void) accessoryDidConnect:(NSNotification *) notification {
    NSLog(@"NOTIFICATION - accessory connected");
    [self processNotification:notification];
}

-(void) accessoryDidDisconnect:(NSNotification *) notification {
    NSLog(@"NOTIFICATION - accessory disconnected");
    [self processNotification:notification];
}

#pragma mark - Background methods
- (void) pollCardStatus {
    @autoreleasepool {
        // Loop infinitely
        _timer_poll_status = [NSTimer scheduledTimerWithTimeInterval:CC_POLLING_PERIOD_S target:self selector:@selector(connectCard) userInfo:nil repeats:YES];
        
        // Set pthread reference
        _pthread_background = pthread_self();
        
        do  {
            [_lock_timeout_counter lock];
            _timeout_counter = 0;
            [_lock_timeout_counter unlock];
            
            // Start the run loop but return after each source is handled.
            SInt32    result = CFRunLoopRunInMode(kCFRunLoopDefaultMode, CC_POLLING_PERIOD_S, YES);
            
            [_lock_timeout_counter lock];
            _timeout_counter = -1;
            [_lock_timeout_counter unlock];
            
            // If a source explicitly stopped the run loop, or if there are no
            // sources or timers, go ahead and exit.
            if(result == kCFRunLoopRunStopped)  {
                NSLog(@"Background thread stopped");
            }
            else if (result == kCFRunLoopRunFinished)  {
                NSLog(@"Background thread runloop finished");
                break;
            }
            else if([[NSThread currentThread] isCancelled]) {
                NSLog(@"Background thread cancelled");
                break;
            }            
        }
        while (true);
        [_timer_poll_status invalidate];

        NSLog(@"pollCardStatus finishing");
    }
    [self resetReader];
}

// Calls connectCard on the reader and sets card and reader status accordingly
- (void) connectCard    {
    long resp = [_reader connectCard];
    NSLog(@"connectCard response %@", [CardCommunication codeToString:resp]);
    if([[NSThread currentThread] isCancelled])   {
        return;
    }
       
    switch (resp) {
        case ID_OK:
            NSLog(@"Don't know how to respond to OK");
            break;
        case ID_READER_ACCESS_SUCCESSFUL:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            break;
        case ID_CARD_ACCESS_SUCCESSFUL:
            [self setCard_status:ID_CARD_ACCESS_SUCCESSFUL];
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            break;
        case ID_CARD_CONNECT_ERROR:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_CARD_ABSENT:
            [self setReader_status:ID_READER_ACCESS_SUCCESSFUL];
            [self setCard_status:ID_CARD_ABSENT];
            break;
        case ID_READER_PROBLEM:
            [self setReader_status:ID_READER_PROBLEM];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_ABSENT:
            [self setReader_status:ID_READER_ABSENT];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_CONTEXT_ERROR:
            [self setReader_status:ID_READER_CONTEXT_ERROR];
            [self setReader_status_error_message:@"Failed to establish context"];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_ERROR:
            [self setReader_status:ID_READER_PROBLEM];
            [self setCard_status:ID_CARD_CONNECT_ERROR];
            break;
        case ID_READER_TRYING:
            [self setReader_status:ID_READER_TRYING];
            break;
        default:
            NSLog(@"Unrecognised response from connectCard");
            break;
    }
    
    [_lock_timeout_counter lock];
    _timeout_counter = -1;
    [_lock_timeout_counter unlock];    
}

#pragma mark - Convenience methods

- (void) tryDetectAlreadyConnectedAccessory {
    if(![NSThread isMainThread])    {
        // Some readers (e.g. Bai) expect accessory detection to take place on the main thread
        [self performSelectorOnMainThread:@selector(tryDetectAlreadyConnectedAccessory) withObject:nil waitUntilDone:NO];
        return;
    }
    EAAccessoryManager *manager = [EAAccessoryManager sharedAccessoryManager];
    for(EAAccessory *acc in [manager connectedAccessories])    {
        NSLog(@"generating a fake EA notification for already connected accessory");
        // Fake generate a ConnectedAccessory notification
        NSDictionary *dict_info = [NSDictionary dictionaryWithObject:acc forKey:EAAccessoryKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:EAAccessoryDidConnectNotification object:nil userInfo:dict_info];
    }
}

// NOTE: This method should be called in the same thread where reader was configured
// (Config creates context which can only be destroyed if called from the same thread)
- (void) resetReader    {
    NSLog(@"+ resetReader");
    [self setReader_status:ID_READER_ABSENT];
    if(_reader) {
        [_reader reset];
        [self setReader:nil];
        
        // Ensure you are still registered for EAAccessory callbacks
        // (might get de-registered during reset of SDK's)
        
        // Register for accessory notifications (first un-register, to be on the safe side. Otherwise, same notification is received multiple times)
        [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidConnect:) name:EAAccessoryDidConnectNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryDidDisconnect:) name:EAAccessoryDidDisconnectNotification object:nil];
        
        [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    }
}

// Called to post status change notification, whenever there is a status change for card or reader
// Ensure that card_status, reader_status, card_status_message and reader_status_message properties are
// set properly before calling this method
- (void) notifyStateChange  {
    NSLog(@"+ notifyStateChange - %@ %@", [CardCommunication readerStatusString:_reader_status] , [CardCommunication cardStatusString:_card_status]);
    
    if([NSThread currentThread] != _thread_current) {
        NSLog(@"Forwarding notifyStateChange to current thread");
        [self performSelector:@selector(notifyStateChange) onThread:_thread_current withObject:nil waitUntilDone:NO];
        return;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:4];
    [dict setObject:[NSNumber numberWithInt:_card_status] forKey:@"card_status"];
    [dict setObject:[NSNumber numberWithInt:_reader_status] forKey:@"reader_status"];
    if(_card_status_error_message)  {
        [dict setObject:_card_status_error_message forKey:@"card_status_error_message"];
    }
    if(_reader_status_error_message)    {
        [dict setObject:_reader_status_error_message forKey:@"reader_status_error_message"];
    }

    
    if(!_is_pooled_notification_mode)    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:CCNOTIFICATION_STATUS_CHANGE object:nil userInfo:dict];
    }
    else    {
        // Invalidate existing notifications
        // (Only send the latest one)
        // NOTE: Beware there is a risk of never sending a notification here (however, if connectCard works properly, then this should never happen. Risk occurs only when the state is constantly changing)

        for(NSTimer *timer in _arr_timed_notifications)   {
            if(timer)   {
                [timer invalidate];
            }
        }
        [_arr_timed_notifications removeAllObjects];

        float time = CC_NOTIFICATION_TIME_WINDOW_DEFAULT;
        if([_reader respondsToSelector:@selector(CC_NOTIFICATION_TIME_WINDOW)])  {
            time = [_reader CC_NOTIFICATION_TIME_WINDOW];
        }
        
        NSTimer *timed_notification = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(fireNotification:) userInfo:dict repeats:NO];
        [_arr_timed_notifications addObject:timed_notification];
    }
}

- (void) fireNotification:(NSTimer *) timer {
    if([timer isValid]) {
        NSLog(@"firing notification");
        [[NSNotificationCenter defaultCenter]
         postNotificationName:CCNOTIFICATION_STATUS_CHANGE object:nil userInfo:[timer userInfo]];
    }
    else    {
        NSLog(@"Notification cancelled");
    }
    timer = nil;
}

- (CardReader *) cardReaderFromAccessoryProtocol:(NSString *) protocol    {
    // Returns CardReader object to handle given External Accessory protocol. nil if protocol is not supported
    protocol = [[CCCommon string_trimmed:protocol] lowercaseString];
    
    NSString *class_name = nil;
    if([protocol isEqualToString:@"com.ewcdma.i-dentity"] )  {
        [CardReader_identity class];
        class_name = @"CardReader_identity";
    }
    else if([protocol isEqualToString:@"com.ewcdma.ecarder"]) {
        [CardReader_identity class];
        class_name = @"CardReader_identity";        
    }
    else if([protocol isEqualToString:@"com.precisebiometrics.ccidcontrol"] ||
            [protocol isEqualToString:@"com.precisebiometrics.ccidinterrupt"] ||
            [protocol isEqualToString:@"com.precisebiometrics.ccidbulk"] ||
            [protocol isEqualToString:@"com.precisebiometrics.sensor"] ||
            [protocol isEqualToString:@"com.precisebiometrics.trace"] ||
            [protocol isEqualToString:@"com.precisebiometrics.manager"]) {
        [CardReader_precise class];
        class_name = @"CardReader_precise";
    }
    else if([protocol isEqualToString:@"com.idtechproducts.ismart"])    {
        [CardReader_idtech class];
        class_name = @"CardReader_idtech";
    }
    else if([protocol isEqualToString:@"com.baimobile.reader"])    {
        [CardReader_bai class];
        class_name = @"CardReader_bai";
    }
    else if([protocol isEqualToString:@"com.ftsafe.crd"] ||
            [protocol isEqualToString:@"com.ftsafe.ir301"])    {
        [CardReader_feitian class];
        class_name = @"CardReader_feitian";
    }
    else    {
        NSLog(@"Unrecognised protocol : %@" , protocol);
    }

    
    if(class_name)  {
        NSLog(@"Instantiate %@", class_name);
        CardReader *reader_object = [[NSClassFromString(class_name) alloc] init];
        [reader_object config];
        return reader_object;
    }
    
    return nil;
}

#pragma mark - Enum to string conversion methods
+ (NSString *) cardStatusString:(long)status  {
    switch (status) {
        case ID_CARD_CONNECT_ERROR:
            return @"card_access_error";
        case ID_CARD_ACCESS_SUCCESSFUL:
            return @"card_access_success";
        case ID_CARD_PRESENT:
            return @"card_detected";
        case ID_CARD_ABSENT:
            return @"card_not_found";
        default:
            return @"(unrecognised ID code)";
    }
}

+ (NSString *) readerStatusString:(long)status  {
    switch (status) {
        case ID_READER_PROBLEM:
            return @"reader_access_error";
        case ID_READER_ACCESS_SUCCESSFUL:
            return @"reader_access_success";
        case ID_READER_PRESENT:
            return @"reader_detected";
        case ID_READER_ABSENT:
            return @"reader_not_found";
        case ID_READER_TRYING:
            return @"reader_access_trying";
        case ID_READER_NOT_VALID:
            return @"reader_not_valid";
        default:
            return @"(unrecognised ID code)";
    }
}

+ (NSString *) codeToString:(ID_CODE)response {
    switch (response) {
        case ID_OK:
            return @"ID_OK";
        case ID_READER_ACCESS_SUCCESSFUL:
            return @"ID_READER_ACCESS_SUCCESSFUL";
        case ID_CARD_ACCESS_SUCCESSFUL:
            return @"ID_CARD_ACCESS_SUCCESSFUL";
        case ID_CARD_CONNECT_ERROR:
            return @"ID_CARD_CONNECT_ERROR";
        case ID_CARD_ABSENT:
            return @"ID_CARD_ABSENT";
        case ID_READER_PROBLEM:
            return @"ID_READER_PROBLEM";
        case ID_READER_ABSENT:
            return @"ID_READER_ABSENT";
        case ID_READER_CONTEXT_ERROR:
            return @"ID_READER_CONTEXT_ERROR";
        case ID_ERROR:
            return @"ID_ERROR";
        case ID_READER_TRYING:
            return @"ID_READER_TRYING";
        default:
            NSLog(@"Unrecognised response from connectCard (%ld)",response);
            break;
    }
    return @"(unrecognised ID code)";
}


@end
