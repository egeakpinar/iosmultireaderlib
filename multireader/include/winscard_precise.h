//
//  winscard_precise.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#ifndef checkid_winscard_precise_h
#define checkid_winscard_precise_h


/** @defgroup g_c_api PCSC winscard.h smart card API
  @{*/
/**
 Initializes the ExternalAccessory framework enabling communications with
 the Precise Biometrics iOS smart card reader.
 @param[in] dwScope Scope of the resource manager context. Only
 SCARD_SCOPE_USER and SCARD_SCOPE_SYSTEM are valid values.
 @param[in] pvReserved1 Reserved for future use and must be NULL.
 @param[in] pvReserved2 Reserved for future use and must be NULL.
 @param[out] phContext A handle to the established resource manager context.
 This handle can now be supplied to other functions attempting to do work
 within this context.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03EstablishContext(DWORD dwScope,
                           LPCVOID pvReserved1,
                           LPCVOID pvReserved2,
                           LPSCARDCONTEXT phContext);

/**
 Releases the session to the accessory and the ExternalAccessory framework
 when the last context is released.
 Any smart card present in the reader will be powered off automatically.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03ReleaseContext(SCARDCONTEXT hContext);

/**
 Establishes a connection between the calling application and a smart card
 contained by a specific reader. If no card exists in the specified reader,
 an error is returned.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @param[in] szReader The name of the reader that contains the target card
 @param[in] dwShareMode A flag that indicates whether other applications may
 form connections to the card. At the moment only SCARD_SHARE_EXCLUSIVE() is
 a valid value.
 @param[in] dwPreferredProtocols A bitmask of acceptable protocols for the
 connection. See \ref g_protocol for valid values.
 @param[out] phCard A handle that identifies the connection to the smart card
 in the designated reader.
 @param[out] pdwActiveProtocol Flag that indicates the established active
 protocol. See \ref g_protocol for valid values.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03Connect(SCARDCONTEXT hContext,
                  LPCSTR szReader,
                  DWORD dwShareMode,
                  DWORD dwPreferredProtocols,
                  LPSCARDHANDLE phCard,
                  LPDWORD pdwActiveProtocol);

/**
 reestablishes an existing connection between the calling application and a
 smart card. This function moves a card handle from direct access to general
 access, or acknowledges and clears an error condition that is preventing
 further access to the card.
 @param[in] hCard Reference value obtained from a previous call to
 SCardConnect().
 @param[in] dwShareMode A flag that indicates whether other applications may
 form connections to the card. At the moment only SCARD_SHARE_EXCLUSIVE is
 a valid value.
 @param[in] dwPreferredProtocols A bitmask of acceptable protocols for the
 connection. See \ref g_protocol for valid values.
 @param[in] dwInitialization Type of initialization that should be performed on
 the card. See \ref g_disposition for valid values.
 @param[out] pdwActiveProtocol Flag that indicates the established active
 protocol. See \ref g_protocol for valid values.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03Reconnect(SCARDHANDLE hCard,
                    DWORD dwShareMode,
                    DWORD dwPreferredProtocols,
                    DWORD dwInitialization,
                    LPDWORD pdwActiveProtocol);

/**
 Terminates a connection previously opened between the calling application
 and a smart card in the target reader.
 @param[in] hCard Reference value obtained from a previous call to
 SCardConnect().
 @param[in] dwDisposition Action to take on the card in the connected reader
 on close. See \ref g_disposition for valid values.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */

LONG SCa03Disconnect(SCARDHANDLE hCard,
                     DWORD dwDisposition);

/**
 provides the current status of a smart card in a reader. You can call it any
 time after a successful call to SCardConnect() and before a successful call
 to SCardDisconnect(). It does not affect the state of the reader or reader
 driver.
 @param[in] hCard Reference value returned from SCardConnect().
 @param[out] mszReaderName List of display names (multiple string) by which
 the currently connected reader is known.
 @param[in,out] pcchReaderLen On input, supplies the length of the
 szReaderName buffer. On output, receives the actual length (in characters)
 of the reader name list, including the trailing NULL character. If this
 buffer length is specified as SCARD_AUTOALLOCATE, then szReaderName is
 converted to a pointer to a byte pointer, and it receives the address of a
 block of memory that contains the multiple-string structure.
 @param[out] pdwState Current state of the smart card in the reader. Upon
 success, it receives one of the following state indicators. See \ref g_card_state
 for valid values.
 @param[out] pdwProtocol Current protocol, if any.
 @param[out] pbAtr Pointer to a 32-byte buffer that receives the ATR string
 from the currently inserted card, if available.
 @param[in,out] pcbAtrLen On input, supplies the length of the pbAtr
 buffer. On output, receives the number of bytes in the ATR string (32 bytes
 maximum). If this buffer length is specified as SCARD_AUTOALLOCATE, then
 pbAtr is converted to a pointer to a byte pointer, and it receives the
 address of a block of memory that contains the multiple-string structure.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03Status(SCARDHANDLE hCard,
                 LPSTR mszReaderName,
                 LPDWORD pcchReaderLen,
                 LPDWORD pdwState,
                 LPDWORD pdwProtocol,
                 LPBYTE pbAtr,
                 LPDWORD pcbAtrLen);

/**
 Blocks execution until the current availability of the cards in a specific
 set of readers changes.
 The caller supplies a list of readers to be monitored by an
 SCARD_READERSTATE array and the maximum amount of time (in milliseconds)
 that it is willing to wait for an action to occur on one of the listed
 readers. Note that SCardGetStatusChange() uses the user-supplied value in
 the dwCurrentState members of the rgReaderStates SCARD_READERSTATE array
 as the definition of the current state of the readers. The function returns
 when there is a change in availability, having filled in the dwEventState
 members of rgReaderStates appropriately.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @param[in] dwTimeout The maximum amount of time, in milliseconds, to wait
 for an action. A value of zero causes the function to return immediately. A
 value of INFINITE causes this function never to time out.
 @param rgReaderStates An array of SCARD_READERSTATE structures that specify
 the readers to watch, and that receives the result.
 @param cReaders The number of elements in the rgReaderStates array.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03GetStatusChange(SCARDCONTEXT hContext,
                          DWORD dwTimeout,
                          LPSCARD_READERSTATE rgReaderStates,
                          DWORD cReaders);
/**
 The SCardTransmit function sends a service request to the smart card and
 expects to receive data back from the card.
 @param[in] hCard Reference value returned from SCardConnect().
 @param[in] pioSendPci pointer to the protocol header structure for the
 instruction. This buffer is in the format of an SCARD_IO_REQUEST structure,
 followed by the specific protocol control information (PCI). For the T=0,
 T=1, and Raw protocols, the PCI structure is constant. The smart card
 subsystem supplies a global T=0, T=1, or Raw PCI structure, which you can
 reference by using the \ref g_pci respectively.
 @param[in] pbSendBuffer A pointer to the actual data to be written to the
 card.
 @param[in] cbSendLength The length, in bytes, of the pbSendBuffer parameter.
 @param[out] pioRecvPci Pointer to the protocol header structure for the
 instruction, followed by a buffer in which to receive any returned protocol
 control information (PCI) specific to the protocol in use. This parameter
 can be NULL if no PCI is returned.
 @param[out] pbRecvBuffer Pointer to any data returned from the card.
 @param[in,out] pcbRecvLength Supplies the length, in bytes, of the
 pbRecvBuffer parameter and receives the actual number of bytes received from
 the smart card. This value cannot be SCARD_AUTOALLOCATE because
 SCardTransmit() does not support SCARD_AUTOALLOCATE.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03Transmit(SCARDHANDLE hCard,
                   const SCARD_IO_REQUEST *pioSendPci,
                   LPCBYTE pbSendBuffer,
                   DWORD cbSendLength,
                   SCARD_IO_REQUEST *pioRecvPci,
                   LPBYTE pbRecvBuffer,
                   LPDWORD pcbRecvLength);

/**
 Provides a list of currently available smart card readers.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @param[in] mszGroups Use a NULL value to list all readers in the system.
 @param[out] mszReaders Multi-string that lists the card readers currently
 connected and available to the system. If this value is NULL,
 SCardListReaders ignores the buffer length supplied in pcchReaders, writes
 the length of the buffer that would have been returned if this parameter had
 not been NULL to pcchReaders, and returns a success code.
 @param[in,out] pcchReaders Length of the mszReaders buffer in characters.
 This parameter receives the actual length of the multi-string structure,
 including all trailing null characters. If the buffer length is specified as
 SCARD_AUTOALLOCATE, then mszReaders is converted to a pointer to a byte
 pointer, and receives the address of a block of memory containing the
 multi-string structure. This block of memory must be deallocated with
 SCardFreeMemory().
 @returns Returns SCARD_S_SUCCESS if at least one reader is connected and
 available to the system. Returns SCARD_E_NO_READERS_AVAILABLE if no reader
 can be found. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03ListReaders(SCARDCONTEXT hContext,
                      LPCSTR mszGroups,
                      LPSTR mszReaders,
                      LPDWORD pcchReaders);

/**
 Releases memory that has been returned from the resource manager using the
 SCARD_AUTOALLOCATE length designator.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @param pvMem Memory block to be released.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03FreeMemory(SCARDCONTEXT hContext,
                     LPCVOID pvMem);

/**
 Cancels an ongoing SCardGetStatusChange() operation. Note that this function
 will cancel operations for all open contexts, not only operations within
 the hContext parameter context.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03Cancel(SCARDCONTEXT hContext);

/**
 Determines whether a context handle is valid.
 @param[in] hContext Context parameter received from a previous call to
 SCardEstablishContext().
 @returns SCARD_S_SUCCESS if the context is valid.
 @returns SCARD_E_INVALID_HANDLE if the context is invalid.
 */
LONG SCa03IsValidContext(SCARDCONTEXT hContext);

/**
 Sets a reader/context attribute.
 Supported attributes are:
 SCARD_ATTR_AUTO_BACKGROUND_HANDLING.
 See the doucment "Precise iOS Toolkit User Manual"
 in the toolkit for further information.
 The behaviour of the smart card library if this functionality
 is disabled and the application does not handle all relevant iOS events is
 undefined. This feature is global. Changing the value will affect all open
 contexts and sessions.
 @param[in] hCard Reference value obtained from a previous call to
 SCardConnect(). Ignored when used together with
 SCARD_ATTR_AUTO_BACKGROUND_HANDLING.
 @param[in] dwAttrId Specifies the identifier for the attribute to set.
 @param[in] pbAttr Pointer to a buffer that supplies the attribute whose
 identifier is supplied in dwAttrId.
 @param[in] cbAttrLen Count of bytes that represent the length of the attribute
 value in the pbAttr buffer.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03SetAttrib(
                    SCARDHANDLE hCard,
                    DWORD dwAttrId,
                    LPCBYTE pbAttr,
                    DWORD cbAttrLen);

/**
 Retrieves a reader/context attribute.
 Supported attributes are:
 SCARD_ATTR_AUTO_BACKGROUND_HANDLING - can be used to the retrieve the current
 state of the automatic background handling.
 @param[in] hCard Reference value obtained from a previous call to
 SCardConnect(). Ignored when used together with
 SCARD_ATTR_AUTO_BACKGROUND_HANDLING.
 @param[in] dwAttrId Specifies the identifier for the attribute to get.
 @param[out] pbAttr Pointer to a buffer that receives the attribute whose ID is
 supplied in dwAttrId. If this value is NULL, SCardGetAttrib ignores the buffer
 length supplied in pcbAttrLen, writes the length of the buffer that would have
 been returned if this parameter had not been NULL to pcbAttrLen, and returns a
 success code.
 @param[in,out] pcbAttrLen Length of the pbAttr buffer in bytes, and receives the
 actual length of the received attribute If the buffer length is specified as
 SCARD_AUTOALLOCATE, then pbAttr is converted to a pointer to a byte pointer,
 and receives the address of a block of memory containing the attribute. This
 block of memory must be deallocated with SCardFreeMemory.
 @returns SCARD_S_SUCCESS if successful. Otherwise an error code is
 returned. See \ref g_error for valid return values
 */
LONG SCa03GetAttrib(
                    SCARDHANDLE hCard,
                    DWORD dwAttrId,
                    LPBYTE pbAttr,
                    LPDWORD pcbAttrLen);


#pragma mark Currently not implemented

/**
 This function is not implemented for iOS.
 @returns Always returns ENOSYS
 */
LONG SCa03ListReaderGroups(SCARDCONTEXT hContext,
                           LPSTR mszGroups,
                           LPDWORD pcchGroups);

/**
 This function is not implemented for iOS.
 @returns Always returns ENOSYS
 */
LONG SCa03Control(SCARDHANDLE hCard,
                  DWORD dwControlCode,
                  LPCVOID pbSendBuffer,
                  DWORD cbSendLength,
                  LPVOID pbRecvBuffer,
                  DWORD cbRecvLength,
                  LPDWORD lpBytesReturned);
/**
 This function is not implemented for iOS.
 @returns Always returns ENOSYS
 */
LONG SCa03BeginTransaction(SCARDHANDLE hCard);
/**
 This function is not implemented for iOS.
 @returns Always returns ENOSYS
 */
LONG SCa03EndTransaction(SCARDHANDLE hCard,
                         DWORD dwDisposition);
/*@}*/

#define SCARD_PCI_T0	(&g_rgSCa03T0Pci) /**< protocol control information (PCI) for T=0 */
#define SCARD_PCI_T1	(&g_rgSCa03T1Pci) /**< protocol control information (PCI) for T=1 */
#define SCARD_PCI_RAW	(&g_rgSCa03RawPci) /**< protocol control information (PCI) for RAW protocol */
extern SCARD_IO_REQUEST g_rgSCa03T0Pci, g_rgSCa03T1Pci, g_rgSCa03RawPci;

#endif
