//
//  CardReader_bai.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 19/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CardReader_bai.h"
#import "CCCommon.h"

// BAI SDK Modules
#import "BAIModuleConnect.h"
#import "BAIModulePairing.h"

// Middleware Module
#import "pkcs11.h"
#import "balPkcs11.h" // Middleware provides a subset of the pkcs#11 interface. See this header for details
//#import "mapPkcs11.h" // Maps normal pkcs#11 names to BAL specific implementation
//#import "pcsclite.h"
#import "winscard_bai.h"
#import "wintypes.h"

#import "IDCodes.h"

#if !(TARGET_IPHONE_SIMULATOR)

@implementation CardReader_bai
@synthesize ATR = _ATR;

#pragma mark - CardReader properties


- (NSString *) getATR {
    EXC_BEGIN
    // Note: bai doesn't support SCardStatus (as stated on baiwiki.com) so this call might be unreliable
    if(!_ATR)   {
        DWORD dwReaderLen = MAX_READERNAME;
        char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
        DWORD dwAtrLen = MAX_ATR_SIZE;
        DWORD dwState,dwProt;
        unsigned char pbAtr[MAX_ATR_SIZE];
        
        LONG rv = SCa01Status(_handle_scard, pcReaders, &dwReaderLen, &dwState, &dwProt,
                              pbAtr, &dwAtrLen);
        NSData *atr_data = [NSData dataWithBytes:pbAtr length:dwAtrLen];
        _ATR = [CCCommon dataToString:atr_data];
    }
    return _ATR;
    EXC_END
}

// NOTE: bai doesn't support destroying context. For this reason, bai uses a single static context for all connections

static hBAIContext *_context_bai;
static unsigned long _hSession;
static SCARDCONTEXT _context_scard; // TODO: Check if they have to be static as well
static SCARDHANDLE _handle_scard;

-(void*)getContext {
    return _context_bai;
}

- (long) verifyPIN:(NSString *)PIN   {
    EXC_BEGIN
    BOOL is_successful = NO;
    unsigned long hSession;
    if (BAL_C_Initialize(NULL)) {
        if ([self p11OpenSession:&hSession] == ID_OK)  {
            // Obtain the PIN, provide it for login
            
            const unsigned char *bValue = (const unsigned char *) [PIN cStringUsingEncoding:NSASCIIStringEncoding];
            
            unsigned int providedPINLength = [PIN length];
            unsigned char* providedPIN = malloc(providedPINLength);
            memcpy(providedPIN, bValue, providedPINLength);
            
            CK_RV rv = BAL_C_Login(hSession,CKU_USER,providedPIN,providedPINLength);
            if(rv == CKR_PIN_LOCKED)    {
                NSLog(@"PIN locked");
            }
            else if(rv == CKR_PIN_INCORRECT)    {
                NSLog(@"PIN incorrect");
            }
            else if(rv == CKR_PIN_INVALID)  {
                NSLog(@"PIN invalid");
            }
            else if(rv == CKR_PIN_EXPIRED)  {
                NSLog(@"PIN expired");
            }
            else if(rv == CKR_OK)   {
                NSLog(@"PIN verification successful");
                is_successful = YES;
            }
            else    {
                NSLog(@"Unexpected PIN response");
            }
            
            BAL_C_CloseSession(hSession);
        }
        BAL_C_Finalize(NULL);
    }
    if(is_successful)   {
        return ID_OK;
    }
    return ID_ERROR;
    EXC_END
}

- (long)connectCard  {
    EXC_BEGIN
    int _readerCount = 0;
    SCARD_READERSTATE _rgReaderStates[1];
    
    LONG rv = SCa01GetStatusChange(_context_scard, 0, _rgReaderStates, _readerCount);
	if(rv!=SCARD_S_SUCCESS) {
        return ID_ERROR;
    } else {
        // Try connect to card here
        if(_rgReaderStates[0].dwEventState & SCARD_STATE_PRESENT)    {
            return ID_CARD_ACCESS_SUCCESSFUL;
        } else {
            NSLog(@"no card");
            
            // Try connect
            bool b = 0;
            // Note: This method takes VERY long (5-8 seconds) when there is no card in the reader
            BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
            if(rval == 0)   {
                NSLog(@"connect successful");
                return ID_CARD_ACCESS_SUCCESSFUL;
            }
            return ID_CARD_ABSENT;
        }
    }
    EXC_END
}


- (long) powerOnCard {
    EXC_BEGIN
	LONG rv = 0;
    DWORD dwActiveProtocol = -1;
    char mszReaders[128] = "";
    DWORD dwReaders = -1;
    
    rv = SCa01ListReaders(_context_scard, NULL, mszReaders, &dwReaders);
    if(rv != SCARD_S_SUCCESS) {
        NSLog(@"SCardListReaders error %08x",rv);
        return ID_ERROR;
    }
    
    NSLog(@"mszReaders %s", mszReaders);
    
    rv = SCa01Connect(_context_scard, mszReaders,
					  SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0,
					  &_handle_scard, &dwActiveProtocol);
    
    switch (dwActiveProtocol) {
        case SCARD_PROTOCOL_ANY:
        case SCARD_PROTOCOL_T0:
            self.active_protocol = PCI_PROTOCOL_0;
            break;
        case SCARD_PROTOCOL_T1:
            self.active_protocol = PCI_PROTOCOL_1;
            break;
        case SCARD_PROTOCOL_RAW:
            self.active_protocol = PCI_PROTOCOL_Raw;
            break;
        default:
            NSLog(@"Unrecognised protocol");
            self.active_protocol = PCI_PROTOCOL_Undefined;
    }
    
	if(rv!=SCARD_S_SUCCESS) {
        // NOTE: For card status, we are always relying on connectCard response for sake of simplicity (separation of concerns)
        NSLog(@"SCardConnect error %x", rv);
        return ID_ERROR;
	}
	else if(!_handle_scard)   {
        return ID_CARD_CONNECT_ERROR;
    }
    
    // Force open bluetooth connection (it's already opened actually)
    bool b = 0;
    // Note: This method takes VERY long (5-8 seconds) when there is no card in the reader
    BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
    if(rval != 0)   {
        NSLog(@"opening bluetooth connection failed %x",rv);
        return ID_ERROR;
    }
        
    return ID_OK;
    /* bai doesn't support SCardStatus (although correct ATR is fetched)
     DWORD dwReaderLen = MAX_READERNAME;
     char* pcReaders = (char *) malloc(sizeof(char) * MAX_READERNAME);
     DWORD dwAtrLen = MAX_ATR_SIZE;
     DWORD dwState,dwProt;
     unsigned char pbAtr[MAX_ATR_SIZE];
     
     rv = SCa01Status(_handle_scard, pcReaders, &dwReaderLen, &dwState, &dwProt,
     pbAtr, &dwAtrLen);
     DDLogCVerbose(@"ATR %@", [NSData dataWithBytes:pbAtr length:dwAtrLen]);
     DDLogCVerbose(@"Current reader protocol %d", dwProt - 1);
     
     if(rv != SCARD_S_SUCCESS) {
     DDLogCVerbose(@"SCardStatus failed %x", rv);
     return cc_response_Error;
     }
     return cc_response_OK;*/
    EXC_END
}


- (long) powerOffCard    {
    EXC_BEGIN
    LONG rv;
    if(_handle_scard)  {
        rv = SCa01Disconnect(_handle_scard, SCARD_RESET_CARD);
        if(rv != CKR_OK)    {
            NSLog(@"SCardDisconnect failed %x", rv);
        }
        _handle_scard = 0;
    }
    // rv is ignored, assuming success
    return ID_OK;
    EXC_END
}

- (Response *) sendCommand:(NSString *)command {
    EXC_BEGIN
    Response *response = [[Response alloc] init];
	uint8_t pbRecvBuffer[512];
	DWORD cbRecvLength=512;
	LONG rv;
	SCARD_IO_REQUEST pioRecvPci;
 	SCARD_IO_REQUEST pioSendPci;
    
    switch (self.active_protocol) {
        case PCI_PROTOCOL_0:
            pioSendPci = *SCARD_PCI_T0;
            break;
        case PCI_PROTOCOL_1:
            pioSendPci = *SCARD_PCI_T1;
            break;
        case PCI_PROTOCOL_Raw:
            pioSendPci = *SCARD_PCI_RAW;
            break;
        case PCI_PROTOCOL_Undefined:
        default:
            NSLog(@"Active protocol not set");
            break;
    }
    
    int pbSendLength = -1;
    uint8_t *pbSendBuffer = [CCCommon stringToByteArray:command outputLength:&pbSendLength];
    if(pbSendBuffer == NULL)    {
        NSLog(@"Could not create command bytes");
        return nil;
	}
    
	rv=SCa01Transmit(_handle_scard,&pioSendPci,
					 pbSendBuffer, pbSendLength,
					 (const unsigned char*)&pioRecvPci,
                     pbRecvBuffer,
					 &cbRecvLength);
    response.rv = rv;
    
	if(rv==SCARD_S_SUCCESS) {
        NSLog(@"SCardTransmit successful");
        NSLog(@"%@",[NSData dataWithBytes:pbRecvBuffer length:cbRecvLength]);
        response.data = [NSData dataWithBytes:pbRecvBuffer length:cbRecvLength-2];
        response.sw1Ptr = pbRecvBuffer[cbRecvLength - 2];
        response.sw2Ptr = pbRecvBuffer[cbRecvLength - 1];
	} else {
        NSLog(@"SCardTransmit Error:%x", rv);
	}
    
    return response;
    EXC_END
}

- (void) config {
    EXC_BEGIN
    super.type = bai;
    if(!_context_bai)   {
        bai_interface_create_context(&_context_bai, NULL);
    }
    if(!_context_scard) {
        LONG rv = SCa01EstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &_context_scard);
        if(rv)  {
            NSLog(@"SCardEstablishContext Error:0x%x",rv);
        }
        else    {
            NSLog(@"SCardEstablishContext Successful");
        }
        
    }
    // The below calls are for the pairing GUI module
    // TODO: Re-think whether you need them
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(pairingFinishedNotificationObserved:)
												 name:BAIPairingDidFinishNotification
											   object:nil];
    EXC_END
}

- (void) reset  {
    EXC_BEGIN
    // NOTE: bai doesn't support destroying bai context
	if(_context_scard)    {
		SCa01ReleaseContext(_context_scard);
        _context_scard = 0;
        _handle_scard = 0;
	}
    EXC_END
}

/*
 - (cc_response) powerOnCard {
 DDLogCVerbose(@"Power ON not implemented for bai, yet");
 return cc_response_Error;
 }
 
 - (cc_response) powerOffCard    {
 DDLogCVerbose(@"Power OFF not implemented for bai, yet");
 return cc_response_Error;
 }*/

#pragma mark - SDK notification handlers

- (void) navigationControllerRequested:(NSNotification*)note {
    EXC_BEGIN
    NSLog(@"nav controller requested");
    // TODO: Current architecture doesn't give you access to the navigation controller. Maybe you should
    // pass in a reference to the nav controller for bai SDK
    //	bai_interface_set_navigation_controller(self.na, [note object]);
    EXC_END
}

- (void) pairingFinishedNotificationObserved:(NSNotification*)note {
    EXC_BEGIN
    NSLog(@"pairingFinishedNotificationObserved");
	BOOL success = [[note object] boolValue];
    EXC_END
}


#pragma mark - p11 methods

- (long) p11OpenSession:(unsigned long *) pSession {
    EXC_BEGIN
    // ** p11OpenSession
    
    CK_SLOT_ID* pSlotList = 0;
    unsigned long ulSlotCount = 0;
    
    // Get slot list for memory allocation
    CK_RV rv = BAL_C_GetSlotList(false, 0, &ulSlotCount);
    
    if ((rv == CKR_OK) && (ulSlotCount > 0))    {
        NSLog(@"  %ld slots found",ulSlotCount);
        pSlotList = (CK_SLOT_ID*)malloc(ulSlotCount * sizeof (CK_SLOT_ID));
        
        if (pSlotList == NULL)  {
            NSLog(@"System error: unable to allocate memory");
            return ID_READER_CONTEXT_ERROR;
        }
        
        // Get the slot list for processing
        rv = BAL_C_GetSlotList(false, pSlotList, &ulSlotCount);
        if (rv != CKR_OK)
        {
            NSLog(@"GetSlotList failed: unable to get slot list. Error code 0x%X",rv);
            free(pSlotList);
            return ID_READER_CONTEXT_ERROR;
        }
    }
    else    {
        NSLog(@"GetSlotList failed: unable to get slot count. Error code 0x%X",rv);
        return ID_READER_CONTEXT_ERROR;
    }
    
    // TODO - use C_GetTokenInfo and check against the flags for the operation we wish to support
    // For now, use the first available slot
    CK_SLOT_ID SlotID;
    if (pSlotList[0] != 0)  {
        SlotID = pSlotList[0];
    }
    else    {
        NSLog(@"Unable to obtain a valid slot ID");
        return ID_READER_CONTEXT_ERROR;
    }
    
    // Establish a bluetooth connection
    bool b = 0;
    BAIRESPONSE rval = bai_interface_connect(_context_bai, 0, NULL, NULL, &b);
    if (rval != 0)  {
        NSLog(@"bai_interface_connect failed 0x%X", rval);
        return ID_READER_PROBLEM;
    }
    
    rv = BAL_C_OpenSession(SlotID,0,NULL,NULL,pSession);
    
    // Free allocated memory
    if (pSlotList)  {
        free(pSlotList);
    }
    
    if(rv == CKR_OK)  {
        return ID_OK;
    }
    return ID_ERROR;
    EXC_END
}

- (void) showPairingGUI:(UINavigationController *)nav_controller callback:(void (BOOL))callback {
	bai_interface_show_pairing_view(_context_bai, nav_controller, callback);
}

#pragma mark - SCard methods (pc/sc)

- (LONG) SCardEstablishContextWithDwScope:(DWORD) dwScope pvReserved1:(LPCVOID) pvReserved1 pvReserved2:(LPCVOID) pvReserved2 phContext: (LPSCARDCONTEXT) phContext  {
    EXC_BEGIN
    return SCa01EstablishContext(dwScope, pvReserved1, pvReserved2, phContext);
    EXC_END
}

- (LONG) SCardIsValidContextWithHContext:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    return SCa01IsValidContext(hContext);
    EXC_END
}

- (LONG) SCardReleaseContextWithHContext:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    return SCa01ReleaseContext(hContext);
    EXC_END
}

- (LONG) SCardSetTimeoutWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout    {
    EXC_BEGIN
    NSLog(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
    EXC_END
}

// TODO: Reuse existing context here
- (LONG) SCardConnectWithHContext:(SCARDCONTEXT) hContext szReader:(LPCSTR) szReader dwShareMode: (DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols phCard:(LPSCARDHANDLE) phCard pdwActiveProtocol:(LPDWORD) pdwActiveProtocol {
    EXC_BEGIN
    return SCa01Connect(hContext, szReader, dwShareMode, dwPreferredProtocols, phCard, pdwActiveProtocol);
    EXC_END
}

- (LONG) SCardReconnectWithHCard:(SCARDHANDLE) hCard dwShareMode:(DWORD) dwShareMode dwPreferredProtocols:(DWORD) dwPreferredProtocols dwInitialization:(DWORD) dwInitialization pdwActiveProtocol:(LPDWORD) pdwActiveProtocol  {
    EXC_BEGIN
    return SCa01Reconnect(hCard, dwShareMode, dwPreferredProtocols, dwInitialization, pdwActiveProtocol);
    EXC_END
}

- (LONG) SCardDisconnectWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    EXC_BEGIN
    return SCa01Disconnect(hCard, dwDisposition);
    EXC_END
}

- (LONG) SCardBeginTransactionWithHCard:(SCARDHANDLE) hCard {
    EXC_BEGIN
    return SCa01BeginTransaction(hCard);
    EXC_END
}

- (LONG) SCardEndTransactionWithHCard:(SCARDHANDLE) hCard dwDisposition:(DWORD) dwDisposition   {
    EXC_BEGIN
    return SCa01EndTransaction(hCard, dwDisposition);
    EXC_END
}

- (LONG) SCardCancelTransactionWithHCard:(SCARDHANDLE) hCard    {
    EXC_BEGIN
    NSLog(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
    EXC_END
}

- (LONG) SCardStatusWithHCard:(SCARDHANDLE) hCard mszReaderNames:(LPSTR) mszReaderNames pcchReaderLen:(LPDWORD) pcchReaderLen pdwState:(LPDWORD) pdwState pdwProtocol:(LPDWORD) pdwProtocol pbAtr:(LPBYTE) pbAtr pcbAtrLen:(LPDWORD) pcbAtrLen  {
    EXC_BEGIN
    return SCa01Status(hCard, mszReaderNames, pcchReaderLen, pdwState, pdwProtocol, pbAtr, pcbAtrLen);
    EXC_END
}

- (LONG) SCardGetStatusChangeWithHContext:(SCARDCONTEXT) hContext dwTimeout:(DWORD) dwTimeout rgReaderStates:(LPSCARD_READERSTATE_A) rgReaderStates cReaders:(DWORD) cReaders   {
    EXC_BEGIN
    return SCa01GetStatusChange(hContext, dwTimeout, rgReaderStates, cReaders);
    EXC_END
}

- (LONG) SCardControlWithHCard:(SCARDHANDLE) hCard dwControlCode:(DWORD) dwControlCode pbSendBuffer:(const void *)pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(void *)pbRecvBuffer cbRecvLength:(DWORD) cbRecvLength lbBytesReturned:(LPDWORD) lpBytesReturned  {
    EXC_BEGIN
    return SCa01Control(hCard, dwControlCode, pbSendBuffer, cbSendLength, pbRecvBuffer, cbRecvLength, lpBytesReturned);
    EXC_END
}

- (LONG) SCardTransmitWithHCard:(SCARDHANDLE) hCard pioSendPci:(LPCSCARD_IO_REQUEST) pioSendPci pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pioRecvPci:(LPSCARD_IO_REQUEST) pioRecvPci pbRecvBuffer:(LPBYTE) pbRecvBuffer pcbRecvLength:(LPDWORD) pcbRecvLength   {
    EXC_BEGIN
    return SCa01Transmit(hCard, pioSendPci, pbSendBuffer, cbSendLength, pioRecvPci, pbRecvBuffer, pcbRecvLength);
    EXC_END
}

- (LONG) SCardSecTransmitWithHCard:(SCARDHANDLE) hCard pbSendBuffer:(LPCBYTE) pbSendBuffer cbSendLength:(DWORD) cbSendLength pbRecvBuffer:(LPBYTE) pbRecvBuffer pbRecvLength:(LPDWORD) pcbRecvLength    {
    EXC_BEGIN
    NSLog(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
    EXC_END
}

- (LONG) SCardListReaderGroupsWithHContext:(DWORD) hContext mszGroups:(LPCBYTE *) mszGroups pcchGroups:(LPDWORD *)pcchGroups    {
    EXC_BEGIN
    NSLog(@"Unimplemented");
    return SCARD_E_READER_UNSUPPORTED;
    EXC_END
}

- (LONG) SCardListReadersWithHContext:(SCARDCONTEXT) hContext mszGroups:(LPCSTR) mszGroups mszReaders:(LPSTR) mszReaders pcchReaders:(LPDWORD) pcchReaders  {
    EXC_BEGIN
    return SCa01ListReaders(hContext, mszGroups, mszReaders, pcchReaders);
    EXC_END
}

- (LONG) SCardCancel:(SCARDCONTEXT) hContext    {
    EXC_BEGIN
    return SCa01Cancel(hContext);
    EXC_END
}

@end
#endif
