//
//  CardReader_idtech.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 14/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardReader.h"
#import <iSmartSDK/iSmartDelegate.h>

@interface CardReader_idtech : CardReader<iSmartDelegate>

@end
