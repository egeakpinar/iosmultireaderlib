//
//  CardReaderProtocol.h
//  SAM
//
//  Created by Olivier Michiels on 04/04/13.
//  Copyright (c) 2013 Olivier Michiels. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import "winscard.h"

@class Response;
@class SAMError;
@class SmartCard;
@class IDManager;

typedef enum {
    PCI_PROTOCOL_0,
    PCI_PROTOCOL_1,
    PCI_PROTOCOL_Raw,
    PCI_PROTOCOL_Undefined
} protocol;

@protocol CardReaderProtocol <NSObject>
-(Response*)sendCommand:(NSString*)command;
@optional
@property (nonatomic, assign) LONG rv;
@property (atomic) NSUInteger simpleStatus;
@property (nonatomic, strong) NSThread* statusThread;
@property (nonatomic, getter = isReaderPresent) BOOL readerPresent;
@property (nonatomic, getter = isCardPresent) BOOL cardPresent;
@property(nonatomic) SCARDCONTEXT hContext;
@property(nonatomic) SCARDHANDLE hCard;
@property(nonatomic) protocol active_protocol;
@property(nonatomic) char* szReader;
@property(nonatomic, getter = isRunning) BOOL running;
@property(nonatomic, strong) NSString *atr;
@property(nonatomic, getter = isPinAlpha) BOOL pinAlpha;
@property (nonatomic, strong) NSArray *cards;
@property(nonatomic, readonly, getter = isSignaturePadded) BOOL signaturePadded;
@property(nonatomic, readonly, getter = needsPairing) BOOL needsPairing;
@property(nonatomic, readonly) uint8_t blockSize;
@property (nonatomic, strong) NSString *readerName;
@property (nonatomic, getter = isValidReader) BOOL validReader;
@property (nonatomic, strong) NSDictionary *license;
@property(nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, strong) EAAccessory *connectedAccessory;
@property (nonatomic, strong) IDManager *myManager;

-(id)initWithParameters:(NSDictionary*)parameters;
-(id)initWithParameters:(NSDictionary *)parameters andConnectedAccessory:(EAAccessory*)connectedAccessory;
-(BOOL)initializeContext;
-(void)releaseContext;
-(void)shutdown;
-(void)update;
-(BOOL)connectCard:(SAMError**)error;
-(void)disConnectCard;
-(SmartCard*)newCard:(SAMError**)error;
-(NSData*)getPhoto:(NSString*)pin error:(SAMError *__autoreleasing *)error;
-(NSDictionary*)getData:(NSString*)pin error:(SAMError *__autoreleasing *)error;
-(BOOL)isDataSecured:(SAMError**)error;
-(NSData*)sign:(NSString*)pin pinStatus:(NSInteger*)pinStatus certificateIndex:(NSInteger)certificateIndex toSign:(NSData*)toSign error:(SAMError**)error;
-(NSData*)getCertificate:(NSInteger)type error:(SAMError**)error;
-(NSUInteger) getReaderStatus;
-(NSInteger)verifyPin:(NSString*)pin error:(SAMError**)error;
-(void)logMessage:(NSString*) message;
-(void)logResponseCode:(Response*)response;
-(void*)getContext;
-(NSArray*)certificatesList:(SAMError**)error;
-(NSArray*)rootCertificatesList:(SAMError**)error;
-(NSArray*)getIdentityFields;
-(NSString*)getAtrFromCard;
-(void)prepareForPairing;
-(void)readerThread:(NSNotification *)note;
-(void)startReaderThread;
-(DWORD)setupReaderState:(SCARD_READERSTATE_A*)readerState;
@end
